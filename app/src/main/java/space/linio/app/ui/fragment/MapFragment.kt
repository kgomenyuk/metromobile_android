package space.linio.app.ui.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import kotlinx.android.synthetic.main.view_progressbar.*
import space.linio.app.R
import space.linio.base.view.BaseFragment


/**
 * Created by Tetawex
 */

class MapFragment : BaseFragment(), OnMapReadyCallback {

    override val layoutId = R.layout.fragment_map

    lateinit var map: GoogleMap
    lateinit var mapView: MapView

    override fun showProgressbar() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressbar() {
        progressbar.visibility = View.GONE
    }

    override fun preInit() {
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): android.view.View? {
        val view = inflater.inflate(layoutId, container, false)
        mapView = view.findViewById(R.id.map_view)
        mapView.onCreate(savedInstanceState)

        //Load map ONLY if permissions required are present
        if (locationPermitted())
            mapView.getMapAsync(this)
        else {
            Toast.makeText(context, getString(R.string.hint_geo_enable), Toast.LENGTH_SHORT).show()
        }
        return setupView(view)
    }

    override fun setupView(view: View): View {
        return view
    }

    override fun postInit() {

    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(map: GoogleMap) {
        this.map = map
        map.isMyLocationEnabled = true
        map.uiSettings.isMyLocationButtonEnabled = true
        map.uiSettings.isMyLocationButtonEnabled = true
        map.uiSettings.isMapToolbarEnabled = true

        // Updates the location and zoom of the MapView
        //val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(43.1, -87.9), 10f)
        //map.animateCamera(cameraUpdate)
        //map.moveCamera(CameraUpdateFactory.newLatLng(LatLng(43.1, -87.9)))
    }

    private fun locationPermitted(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity!!.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 1)
                return false
            }
        }
        return true
    }

    override fun onResume() {
        mapView.onResume()
        super.onResume()
    }


    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    companion object {

        fun newInstance(): androidx.fragment.app.Fragment {
            return MapFragment()
        }
    }
}