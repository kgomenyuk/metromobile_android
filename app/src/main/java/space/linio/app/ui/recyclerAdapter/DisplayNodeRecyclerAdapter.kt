package space.linio.app.ui.recyclerAdapter

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import space.linio.app.R
import space.linio.app.presenter.ddo.DisplayNodeData
import space.linio.base.view.BaseRecyclerAdapter

class DisplayNodeRecyclerAdapter(context: Context) :
    BaseRecyclerAdapter<DisplayNodeData, DisplayNodeRecyclerAdapter.ViewHolder>(context) {
    override val layoutId = R.layout.recycleritem_route_node

    override fun bindSingleItem(viewHolder: ViewHolder, item: DisplayNodeData) {
        viewHolder.tvDescription.text = item.description
        //viewHolder.tvEstimatedTime.text = item.estimatedArrivalTime.hourOfDay().toString()
        viewHolder.tvEstimatedTime.text = "4:20"
        //TODO Add icon selection based on routeNodeType value
        //when(item.routeNodeType)
        viewHolder.ivNodeType.setImageResource(R.drawable.ic_directions_subway_black_24dp)
    }

    override fun createVH(view: View): ViewHolder {
        return ViewHolder(view)
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
//        @BindView(R.id.tv_estimated_time)
        lateinit var tvEstimatedTime: TextView

//        @BindView(R.id.tv_description)
        lateinit var tvDescription: TextView

//        @BindView(R.id.iv_node_type)
        lateinit var ivNodeType: ImageView

        init {
//            ButterKnife.bind(this, itemView)
        }
    }
}