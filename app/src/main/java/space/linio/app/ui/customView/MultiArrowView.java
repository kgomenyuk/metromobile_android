package space.linio.app.ui.customView;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import java.util.Collections;
import java.util.List;

import space.linio.app.R;
import space.linio.app.model.dto.data.Arrow;

import static android.content.Context.SENSOR_SERVICE;

/**
 * TODO: document your custom view class.
 */
public class MultiArrowView extends View {
    int paddingLeft;
    int paddingTop;
    int paddingRight;
    int paddingBottom;
    int contentWidth;
    int contentHeight;
    int contentCenterX;
    int contentCenterY;
    private float fontSize = 0;
    private Drawable arrowDrawable;
    private Arrow northArrow = new Arrow(0f, "N");
    private List<Arrow> arrowList = Collections.emptyList();
    private TextPaint mTextPaint;
    private float mTextWidth;
    private float mTextHeight;
    private float arrowHeight = 800;
    private float arrowWidth = 141;
    private float deviceRotation = 0f;
    private SensorManager mSensorManager;
    private SensorEventListener sensorEventListener;


    public MultiArrowView(Context context) {
        super(context);
        init(null, 0);
    }

    public MultiArrowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public MultiArrowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    public List<Arrow> getArrowList() {
        return arrowList;
    }

    public void setArrowList(List<Arrow> arrowList) {
        this.arrowList = arrowList;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mSensorManager = (SensorManager) getContext().getSystemService(SENSOR_SERVICE);
        sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                float angle = Math.round(sensorEvent.values[0]);
                deviceRotation = angle;
                invalidate();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };
        if (getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS))
            mSensorManager.registerListener(sensorEventListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                    SensorManager.SENSOR_DELAY_GAME);

    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mSensorManager.unregisterListener(sensorEventListener);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.MultiArrowView, defStyle, 0);

        // Use getDimensionPixelSize or getDimensionPixelOffset when dealing with
        // values that should fall on pixel boundaries.
        fontSize = a.getDimension(
                R.styleable.MultiArrowView_fontSize,
                fontSize);

        if (a.hasValue(R.styleable.MultiArrowView_arrowDrawable)) {
            arrowDrawable = a.getDrawable(
                    R.styleable.MultiArrowView_arrowDrawable);
            arrowDrawable.setCallback(this);
        }

        a.recycle();

        // Set up a default TextPaint object
        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.LEFT);

        // Update TextPaint and text measurements from attributes
        invalidateTextPaintAndMeasurements();
    }

    private void invalidateTextPaintAndMeasurements() {
        mTextPaint.setTextSize(fontSize);
        //mTextPaint.setColor(Color.BLUE);
        //mTextWidth = mTextPaint.measureText("");

        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        mTextHeight = fontMetrics.bottom;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paddingLeft = getPaddingLeft();
        paddingTop = getPaddingTop();
        paddingRight = getPaddingRight();
        paddingBottom = getPaddingBottom();

        contentWidth = getWidth() - paddingLeft - paddingRight;
        contentHeight = getHeight() - paddingTop - paddingBottom;
        contentCenterX = contentWidth / 2 + paddingLeft;
        contentCenterY = contentHeight / 2 + paddingTop;
        arrowHeight = Math.min(contentCenterX * 0.7f, contentCenterY * 0.7f);

        drawArrow(canvas, northArrow);
        for (Arrow arrow : arrowList) {
            drawArrow(canvas, arrow);
        }
    }

    private void drawArrow(Canvas canvas, Arrow arrow) {
        canvas.save();
        canvas.rotate(arrow.getAngle() + deviceRotation, contentCenterX, contentCenterY);
        canvas.drawText(arrow.getTitle(), contentCenterX, contentCenterY - arrowHeight * 1.3f, mTextPaint);
        if (arrowDrawable != null) {
            arrowDrawable.setBounds((int) (contentCenterX - arrowWidth / 2), (int) (contentCenterY - arrowHeight * 1.2f),
                    (int) (contentCenterX + arrowWidth / 2), contentCenterY);
            arrowDrawable.draw(canvas);

        }
        canvas.restore();
    }

    public float getFontSize() {
        return fontSize;
    }

    public void setFontSize(float fontSize) {
        this.fontSize = fontSize;
    }

    public Drawable getArrowDrawable() {
        return arrowDrawable;
    }

    public void setArrowDrawable(Drawable arrowDrawable) {
        this.arrowDrawable = arrowDrawable;
    }

    public float getDeviceRotation() {
        return deviceRotation;
    }

    public void setDeviceRotation(float deviceRotation) {
        this.deviceRotation = deviceRotation;
    }

    public float getNorthRotation() {
        return northArrow.getAngle();
    }

    public void setNorthRotation(float rotation) {
        northArrow.setAngle(rotation);
    }
}
