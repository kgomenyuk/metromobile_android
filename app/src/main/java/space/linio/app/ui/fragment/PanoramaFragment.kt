package space.linio.app.ui.fragment

import android.graphics.*
import android.hardware.SensorManager
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.asha.vrlib.MDVRLibrary
import com.asha.vrlib.model.MDHotspotBuilder
import com.asha.vrlib.model.MDPinchConfig
import com.asha.vrlib.model.MDPosition
import com.asha.vrlib.model.MDRay
import com.asha.vrlib.plugins.MDWidgetPlugin
import com.asha.vrlib.plugins.hotspot.IMDHotspot
import com.asha.vrlib.texture.MD360BitmapTexture
import com.bumptech.glide.Glide
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.fragment_panorama.*
import kotlinx.android.synthetic.main.view_progressbar.*
import space.linio.app.R
import space.linio.app.model.api.PanoramaConfig
import space.linio.app.model.dto.data.graph.connected.Node
import space.linio.app.presenter.PanoramaPresenter
import space.linio.app.view.PanoramaView
import space.linio.base.view.BaseFragment
import java.io.FileNotFoundException

class PanoramaFragment : BaseFragment(), PanoramaView {
    private var panoramaUrl = ""
    private var enablePanoramaCrop = true

    override fun showCantGoBackHint() {
        Toast.makeText(context, getString(R.string.hint_cant_go_back), Toast.LENGTH_SHORT).show()
    }

    override fun setDisplayNode(node: Node) {
        Log.d("image url", node.displayData.imageUrl)
        //setup new image
        panoramaUrl = node.displayData.imageUrl
        if (!node.displayData.isPanorama) {
            mVRLibrary.switchProjectionMode(activity, MDVRLibrary.PROJECTION_MODE_PLANE_CROP)
            enablePanoramaCrop = false
        } else {
            mVRLibrary.switchProjectionMode(
                activity,
                MDVRLibrary.PROJECTION_MODE_STEREO_SPHERE_VERTICAL
            )
            enablePanoramaCrop = true
        }

        //replace plugins
        mVRLibrary.removePlugins()
        node.edges.forEach { edge ->
            addMDVRPlugin(
                node.displayData.isPanorama, edge.displayPosition.y / PanoramaConfig.panoramaWidth
            ) {
                presenter.onNavigationPerformed(edge.toId)
            }
        }
        mVRLibrary.notifyPlayerChanged()
        mVRLibrary.setPinchScale(1.25f)
        mVRLibrary.updateCamera()
    }

    override val layoutId = R.layout.fragment_panorama

    lateinit var mVRLibrary: MDVRLibrary


    override fun showProgressbar() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressbar() {
        progressbar.visibility = View.GONE
    }

    override fun preInit() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @InjectPresenter(type = PresenterType.GLOBAL, tag = "panorama")
    lateinit var presenter: PanoramaPresenter


    override fun setupView(view: View): View {
        return view
    }

    private var vrLibReady = false


    override fun postInit() {
        btn_back.setOnClickListener {
            presenter.onGoBackPerformed()
        }

        if (vrLibReady) return
        mVRLibrary = createVRLibrary()
        //loadImage(target = panoramaBitmapTarget,url = "")
        vrLibReady = true
    }

    override fun onResume() {
        super.onResume()
        mVRLibrary.onResume(activity)
    }

    override fun onPause() {
        super.onPause()
        mVRLibrary.onPause(activity)
    }

    companion object {

        fun newInstance(): androidx.fragment.app.Fragment {
            return PanoramaFragment()
        }
    }

    private fun createVRLibrary(): MDVRLibrary {
        return MDVRLibrary.with(activity)
            .motionDelay(SensorManager.SENSOR_DELAY_GAME)
            .pinchConfig(MDPinchConfig().setMin(1.25f).setMax(4f).setDefaultValue(1.25f))
            .pinchEnabled(true)
            .displayMode(MDVRLibrary.DISPLAY_MODE_NORMAL)
            .interactiveMode(MDVRLibrary.INTERACTIVE_MODE_TOUCH)
            .asBitmap { callback ->
                val panoramaBitmapTarget = (object : SimpleTarget<Bitmap>() {
                    override fun onResourceReady(
                        bitmap: Bitmap,
                        glideAnimation: GlideAnimation<in Bitmap>?
                    ) {
                        if (enablePanoramaCrop) {
                            //turning cylindrical panorama into a spherical one
                            val result = Bitmap.createBitmap(
                                bitmap.width,
                                Math.round(bitmap.width / 1f),
                                Bitmap.Config.ARGB_8888
                            )
                            val canvas = Canvas(result)

                            val paint = Paint()
                            paint.color = Color.BLACK
                            canvas.drawRect(0f, 0f, bitmap.width + 0f, bitmap.width / 1f, paint)

                            val offset =
                                ((canvas.height / 2f - bitmap.height / 2f) / 2.5f) //2.5f is a magic number
                            canvas.drawBitmap(bitmap, 0f, offset, null)

                            mVRLibrary.onTextureResize(result.width + 0f, result.height + 0f)
                            mVRLibrary.updateCamera()

                            callback.texture(result)
                        } else
                            callback.texture(bitmap)
                    }
                })
                loadImage(panoramaUrl, panoramaBitmapTarget)
                //presenter.onNavigationPerformed(2579)
            }
            //.listenTouchPick { hitHotspot, ray -> Log.d(TAG, "Ray:$ray, hitHotspot:$hitHotspot") }
            .projectionMode(MDVRLibrary.PROJECTION_MODE_STEREO_SPHERE_VERTICAL)
            .directorFilter(object : MDVRLibrary.DirectorFilterAdatper() {
                override fun onFilterPitch(input: Float): Float {
                    // limit from -70 to 70
                    if (input > 0) {
                        return 0f
                    }

                    return if (input < -0) {
                        -0f
                    } else input

                }

                override fun onFilterRoll(input: Float): Float {
                    // limit from -70 to 70
                    if (input > 0) {
                        return 0f
                    }

                    return if (input < -0) {
                        -0f
                    } else input

                }

                override fun onFilterYaw(input: Float): Float {
                    return super.onFilterYaw(input)
                }
            })
            .pinchEnabled(true)
            .build(surface_view)
    }

    private fun getDrawableUrl(): String {
        return "https://linio.space/content/img/547"
    }


    private fun loadImage(url: String, target: Target<Bitmap>) {
        Glide.with(activity)
            .load(url)
            .asBitmap()
            .placeholder(R.drawable.ic_device_hub_black_24dp)
            .into(target)
    }

    private fun addMDVRPlugin(isPanorama: Boolean, yaw: Float, clickAction: () -> Unit) {
        val position = MDPosition.newInstance()
            .setZ(
                when (isPanorama) {
                    true -> -10f
                    false -> -1f
                }
            )
            .setYaw(yaw)
        val size = 0.75f * when (isPanorama) {
            true -> 4f
            false -> 0.25f
        }
        val builder = MDHotspotBuilder.create(
            { uri: Uri, callback: MD360BitmapTexture.Callback ->
                try {
                    val bitmap =
                        BitmapFactory.decodeStream(activity!!.contentResolver.openInputStream(uri))
                    callback.texture(bitmap)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }
            })
            .size(size, size)
            //.provider(0, activity, android.R.drawable.star_off)
            .provider(1, activity, R.drawable.logo)
            //.provider(10, activity, android.R.drawable.checkbox_off_background)
            //.provider(11, activity, android.R.drawable.checkbox_on_background)
            .listenClick(object : MDVRLibrary.ITouchPickListener {
                override fun onHotspotHit(hotspot: IMDHotspot?, p1: MDRay?) {
                    if (hotspot is MDWidgetPlugin) {
                        clickAction.invoke()
                    }
                }

            })
            .title("test")
            .position(position)
            .status(1, 1)
        //.checkedStatus(10, 11)

        val plugin = MDWidgetPlugin(builder)

        mVRLibrary.addPlugin(plugin)
    }
}
