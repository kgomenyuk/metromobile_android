package space.linio.app.ui.customView.stationTopDown.entity

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import space.linio.base.view.customView.BaseEntity
import space.linio.base.view.customView.DrawableEntity
import space.linio.base.view.customView.Entity
import space.linio.base.view.customView.Transform
import kotlin.math.min
import kotlin.math.roundToInt

class PlatformExitEntity(
    name: String,
    transform: Transform,
    parent: Entity,
    elevation: Float,
    private val drawable: Drawable
) : BaseEntity(name, transform, parent, elevation), DrawableEntity {

    private val textPaint = TextPaint().apply {
        color = Color.BLACK
        isAntiAlias = true
    }

    override fun draw(canvas: Canvas) {
        pixelBounds.run {
            drawable.setBounds(x, y, x + width, y + height)
            drawable.draw(canvas)

            canvas.save()

            val dimension = min(width, height)

            val padding = dimension * 0.1f

            canvas.translate(x + padding, y + padding)

            textPaint.textSize = dimension * 0.2f

            StaticLayout(
                name, textPaint, width - (padding * 2).roundToInt(),
                Layout.Alignment.ALIGN_LEFT,
                0.9f, 0f, false
            ).draw(canvas)

            canvas.restore()
        }
    }
}