package space.linio.app.ui.fragment

import androidx.fragment.app.Fragment
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import kotlinx.android.synthetic.main.fragment_top_down.*
import kotlinx.android.synthetic.main.view_progressbar.*
import space.linio.app.R
import space.linio.app.model.dto.data.stationschema.Floor
import space.linio.app.presenter.TopDownPresenter
import space.linio.app.ui.customView.stationTopDown.StationTopDownView
import space.linio.app.view.TopDownView
import space.linio.base.view.BaseFragment

/**
 * Created by Tetawex
 */

class TopDownFragment : TopDownView, BaseFragment() {

    @InjectPresenter(type = PresenterType.GLOBAL, tag = "top_down")
    lateinit var presenter: TopDownPresenter

    private lateinit var viewStation: StationTopDownView

    override fun setStationFloorSchema(floor: Floor) {
        view_top_down.setStationFloorSchema(floor)
    }

    override val layoutId = R.layout.fragment_top_down

    override fun showProgressbar() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressbar() {
        progressbar.visibility = View.GONE
    }

    override fun preInit() {
    }

    override fun setupView(view: View): View {
//        viewStation = view.findViewById(R.id.view_top_down)
        return view
    }

    override fun postInit() {
    }

    companion object {

        fun newInstance(): androidx.fragment.app.Fragment {
            return TopDownFragment()
        }
    }
}