package space.linio.app.ui.customView.stationTopDown.entity

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.Drawable
import space.linio.base.view.customView.*
import kotlin.math.roundToInt

class TrainLineEntity(
    override val name: String,
    override var transform: Transform,
    override var parent: Entity?,
    private val backgroundPaint: Paint,
    private val regularWagonDrawable: Drawable,
    private val headWagonDrawable: Drawable,
    private val tailWagonDrawable: Drawable
) : BaseEntity(name, transform, parent), DrawableEntity, ClickableEntity {

    private var clickListener: (Float, Float) -> Unit = { _, _ -> }

    override fun draw(canvas: Canvas) {
        pixelBounds.run {
            val wagonHeight = (height * 0.2f).roundToInt()
            val wagonWidth = (wagonHeight / 2.7f).roundToInt()
            val wagonOffset = (wagonWidth * 0.1f).roundToInt()
            val wagonCount = (height / (wagonHeight + wagonOffset)).toInt()

            val wagonChainYOffset = (height - wagonCount * (wagonHeight + wagonOffset)) / 2

            canvas.drawRect(
                x.toFloat(), y.toFloat(), x + width.toFloat(),
                y + height.toFloat(), backgroundPaint
            )

            val wagonXOffset = (width / 2f - wagonWidth / 2f).roundToInt()

            fun drawWagon(
                i: Int,
                useHeadDrawable: Boolean = false,
                flipDrawable: Boolean = false
            ) {

                val drawable = if (!useHeadDrawable) regularWagonDrawable
                else if (flipDrawable) tailWagonDrawable else headWagonDrawable

                drawable.setBounds(
                    x + wagonXOffset,
                    y + wagonChainYOffset + (wagonHeight + wagonOffset) * i,
                    x + wagonXOffset + wagonWidth,
                    y + wagonChainYOffset + (wagonHeight + wagonOffset) * i + wagonHeight
                )
                drawable.draw(canvas)
            }

            //Draw head/tail
            drawWagon(0, true, false)
            drawWagon(wagonCount - 1, true, true)

            //Draw the rest
            for (i in 1 until wagonCount - 1) drawWagon(i)
        }
    }

    override fun invokeOnClick(x: Float, y: Float) {
        clickListener(x, y)
    }

    override fun setOnClickListener(listener: (Float, Float) -> Unit) {
        clickListener = listener
    }
}