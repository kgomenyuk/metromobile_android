package space.linio.app.ui.customView.stationTopDown.entity

import android.graphics.Canvas
import android.graphics.Paint
import space.linio.base.view.customView.BaseEntity
import space.linio.base.view.customView.DrawableEntity
import space.linio.base.view.customView.Entity
import space.linio.base.view.customView.Transform

class PlatformEntity(
    name: String,
    transform: Transform,
    parent: Entity,
    private val paint: Paint
) : BaseEntity(name, transform, parent), DrawableEntity {

    override fun draw(canvas: Canvas) {
        pixelBounds.run {
            canvas.drawRect(
                x.toFloat(),
                y.toFloat(),
                (x + width).toFloat(),
                (y + height).toFloat(),
                paint
            )
        }
    }
}