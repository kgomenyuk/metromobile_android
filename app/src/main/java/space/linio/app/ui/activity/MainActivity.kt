package space.linio.app.ui.activity

import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_main.*
import space.linio.app.R
import space.linio.app.presenter.MainPresenter
import space.linio.app.ui.fragment.*
import space.linio.app.view.MainView
import space.linio.app.view.ScreenTag
import space.linio.base.view.BaseActivity

/**
 * Created by tetawex on 03.11.17.
 */
class MainActivity : BaseActivity(), MainView {

    override val layoutId: Int
        get() = R.layout.activity_main

    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    private var fragmentManager: androidx.fragment.app.FragmentManager = supportFragmentManager

    private val bottomNavigationListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            mainPresenter.onTabSelected(toScreenTag(item.itemId))
            true
        }

    override fun setScreen(screenTag: ScreenTag) {
        val tag = ScreenTag.toString(screenTag)
        when (screenTag) {
            ScreenTag.SETTINGS ->
                changeFragment(
                    fragment = SettingsFragment.newInstance(),
                    tag = tag,
                    addToBackStack = false
                )
            ScreenTag.PANORAMA ->
                changeFragment(
                    fragment = PanoramaFragment.newInstance(),
                    tag = tag,
                    addToBackStack = false
                )
            ScreenTag.TOP_DOWN ->
                changeFragment(
                    fragment = TopDownFragment.newInstance(),
                    tag = tag,
                    addToBackStack = false
                )
            ScreenTag.ROUTE ->
                changeFragment(
                    fragment = RouteFragment.newInstance(),
                    tag = tag,
                    addToBackStack = false
                )
            ScreenTag.MAP ->
                changeFragment(
                    fragment = MapFragment.newInstance(),
                    tag = tag,
                    addToBackStack = false
                )
            else ->
                changeFragment(
                    fragment = PanoramaFragment.newInstance(),
                    tag = tag,
                    addToBackStack = false
                )
        }
    }

    override fun setupView() {
        bnv_bottom.setOnNavigationItemSelectedListener(bottomNavigationListener)
    }

    override fun onBackPressed() {
        mainPresenter.onBackStackPopped()
        //fragmentManager.popBackStack();
    }

    //copypasted code
    private fun changeFragment(fragment: androidx.fragment.app.Fragment, tag: String, addToBackStack: Boolean) {

        val existingFragment = fragmentManager.findFragmentByTag(tag)

        if (existingFragment == null) {
            // fragment is not in the backstack, create it.
            val ft = fragmentManager.beginTransaction()
            ft.replace(R.id.fragment_placeholder, fragment, tag)
            if (addToBackStack) ft.addToBackStack(tag)
            ft.commit()
        } else {
            // fragment is in the backstack, call it back.
            val ft = fragmentManager.beginTransaction()

            ft.replace(R.id.fragment_placeholder, existingFragment, tag)
            if (addToBackStack) {
                fragmentManager.popBackStack(tag, 0)
                //ft.addToBackStack(tag);
            }
            ft.commit()
        }
    }

    private fun toId(tag: ScreenTag): Int {
        return when (tag) {
            ScreenTag.MAP -> R.id.nav_map
            ScreenTag.PANORAMA -> R.id.nav_panorama
            ScreenTag.TOP_DOWN -> R.id.nav_top_down
            ScreenTag.ROUTE -> R.id.nav_route
            ScreenTag.SETTINGS -> R.id.nav_settings
        }
    }

    private fun toScreenTag(id: Int): ScreenTag {
        return when (id) {
            R.id.nav_map -> ScreenTag.MAP
            R.id.nav_panorama -> ScreenTag.PANORAMA
            R.id.nav_top_down -> ScreenTag.TOP_DOWN
            R.id.nav_route -> ScreenTag.ROUTE
            R.id.nav_settings -> ScreenTag.SETTINGS
            else -> ScreenTag.SETTINGS
        }
    }

    override fun showProgressbar() {

    }

    override fun hideProgressbar() {

    }

    override fun preInit() {
    }

    override fun postInit() {
    }
}