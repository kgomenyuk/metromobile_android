package space.linio.app.ui.activity

import android.content.Intent
import android.content.res.Configuration
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.view_progressbar.*
import space.linio.app.R
import space.linio.app.presenter.LoginPresenter
import space.linio.app.service.NavigationGraphService
import space.linio.app.view.LoginView
import space.linio.base.view.BaseActivity


/**
 * Created by tetawex on 11.01.2018.
 */
class LoginActivity : BaseActivity(), LoginView {

    @InjectPresenter
    lateinit var loginPresenter: LoginPresenter

    override val layoutId: Int = R.layout.activity_login

    override fun setupView() {
        //hide logo if in landscape orientation
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
            iv_logo.visibility = View.GONE
        //bind login button listener
        btn_login.setOnClickListener {
            loginPresenter.onLoginActionPerformed(
                et_login.text.toString(),
                et_password.text.toString()
            )
        }
        tv_register.setOnClickListener {
            loginPresenter.onRegisterActionPerformed()
        }
    }

    override fun preInit() {
    }

    override fun postInit() {
    }

    override fun showProgressbar() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressbar() {
        progressbar.visibility = View.GONE
    }

    override fun navigateToMainScreen() {
        startService(Intent(this, NavigationGraphService::class.java))
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun navigateToRegisterScreen() {
        startActivity(Intent(this, RegisterActivity::class.java))
    }
}