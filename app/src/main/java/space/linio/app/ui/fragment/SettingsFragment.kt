package space.linio.app.ui.fragment

import androidx.fragment.app.Fragment
import android.view.View
import kotlinx.android.synthetic.main.view_progressbar.*
import space.linio.app.R
import space.linio.base.view.BaseFragment

/**
 * Created by Tetawex
 */

class SettingsFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_settings

    override fun showProgressbar() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressbar() {
        progressbar.visibility = View.GONE
    }

    override fun preInit() {
    }

    override fun setupView(view: View): View {
        return view
    }

    override fun postInit() {

    }

    companion object {

        fun newInstance(): androidx.fragment.app.Fragment {
            return SettingsFragment()
        }
    }
}
