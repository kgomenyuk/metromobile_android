package space.linio.app.ui.fragment

import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.fragment_route.*
import kotlinx.android.synthetic.main.view_progressbar.*
import space.linio.app.R
import space.linio.app.presenter.RoutePresenter
import space.linio.app.presenter.ddo.DisplayNodeData
import space.linio.app.ui.recyclerAdapter.DisplayNodeRecyclerAdapter
import space.linio.app.view.RouteView
import space.linio.base.view.BaseFragment

/**
 * Created by Tetawex on 30.10.2017.
 */

class RouteFragment : BaseFragment(), RouteView {
    @InjectPresenter
    lateinit var presenter: RoutePresenter;

    override val layoutId = R.layout.fragment_route


    private lateinit var displayNodeRecyclerAdapter: DisplayNodeRecyclerAdapter

    override fun setNodeList(data: List<DisplayNodeData>) {
        displayNodeRecyclerAdapter.replaceDataWithNotify(data)
    }

    override fun displayEmptyRouteHint() {
        tv_hint_empty_ets.visibility = View.VISIBLE
    }

    override fun hideEmptyRouteHint() {
        tv_hint_empty_ets.visibility = View.GONE
    }

    override fun preInit() {
        displayNodeRecyclerAdapter = DisplayNodeRecyclerAdapter(context!!)
    }

    override fun postInit() {
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        rv_route_node_list.adapter = displayNodeRecyclerAdapter
        rv_route_node_list.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(activity)
        et_from.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable) {
                presenter.onFromQueryChanged(editable.toString())
            }

            override fun beforeTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })
        et_to.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable) {
                presenter.onToQueryChanged(editable.toString())
            }

            override fun beforeTextChanged(text: CharSequence, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence, p1: Int, p2: Int, p3: Int) {
            }

        })
    }

    override fun showProgressbar() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressbar() {
        progressbar.visibility = View.GONE
    }

    companion object {

        fun newInstance(): androidx.fragment.app.Fragment {
            return RouteFragment()
        }
    }
}
