package space.linio.app.ui.customView.stationTopDown

import android.content.Context
import android.content.Context.SENSOR_SERVICE
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.AttributeSet
import space.linio.app.R
import space.linio.app.model.dto.data.stationschema.Floor
import space.linio.app.model.dto.data.stationschema.objects.Platform
import space.linio.app.model.dto.data.stationschema.objects.TrainWay
import space.linio.app.model.dto.data.stationschema.objects.Wall
import space.linio.app.ui.customView.stationTopDown.entity.*
import space.linio.base.view.customView.Entity
import space.linio.base.view.customView.SceneView
import space.linio.base.view.customView.Transform
import space.linio.base.view.customView.Vector2

class StationTopDownView(context: Context, attrs: AttributeSet?, defStyle: Int) :
    SceneView(context, attrs, defStyle) {

    private val headWagonDrawable: Drawable = context.resources.getDrawable(R.drawable.wagon_head)
    private val regularWagonDrawable: Drawable =
        context.resources.getDrawable(R.drawable.wagon_regular)
    private val tailWagonDrawable: Drawable = context.resources.getDrawable(R.drawable.wagon_tail)

    private val stationRectangleNinePatchDrawable =
        context.resources.getDrawable(R.drawable.rectangle)
    private val navStartPointDrawable: Drawable =
        context.resources.getDrawable(R.drawable.nav_point)

    private val trainWayPaint = Paint().apply {
        style = Paint.Style.FILL
        color = Color.GRAY
    }

    private val platformPaint = Paint().apply {
        style = Paint.Style.FILL
        color = Color.LTGRAY
    }

    private val wallPaint = Paint().apply {
        style = Paint.Style.FILL
        color = Color.DKGRAY
    }

    private lateinit var sensorManager: SensorManager
    private lateinit var sensorEventListener: SensorEventListener
    private var deviceRotation: Float = 0f

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    init {
        //Stubs
        /*scene.addEntity(TrainLineEntity(
                name = "train1",
                headWagonDrawable = headWagonDrawable,
                regularWagonDrawable = regularWagonDrawable,
                tailWagonDrawable = tailWagonDrawable,
                parent = scene,
                applyTransform = Transform(Vector2(0f, 0f), Vector2(0.3f, 1f))
        ))
        scene.addEntity(TrainLineEntity(
                name = "train2",
                headWagonDrawable = headWagonDrawable,
                regularWagonDrawable = regularWagonDrawable,
                tailWagonDrawable = tailWagonDrawable,
                parent = scene,
                applyTransform = Transform(Vector2(0.5f, 0.5f), Vector2(0.3f, 0.4f))
        ))*/
    }

    fun setStationFloorSchema(floor: Floor) {
        overrideSceneFromStationFloorSchema(floor)
    }

    public override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        attachSensors()
    }

    public override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        detachSensors()
    }

    private fun attachSensors() {
        if (isInEditMode) return

        //Attach a rotation sensor
        sensorManager = context.getSystemService(SENSOR_SERVICE) as SensorManager
        sensorEventListener = object : SensorEventListener {
            override fun onSensorChanged(sensorEvent: SensorEvent) {
                val angle = Math.round(sensorEvent.values[0]).toFloat()
                deviceRotation = angle
                invalidate()
            }

            override fun onAccuracyChanged(sensor: Sensor, i: Int) {

            }
        }

        //Register a listener if the system has the required feature
        if (context.packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS)) {
            sensorManager.registerListener(
                sensorEventListener,
                sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME
            )
        }
    }

    private fun detachSensors() {
        if (isInEditMode) return

        sensorManager.unregisterListener(sensorEventListener)
    }

    private fun overrideSceneFromStationFloorSchema(floor: Floor) {
        scene.clear()

        var floorAggregatedXOffset = 0f

        floor.objects.sortedBy { it.orderNumber }.forEach { floorObject ->
            val entity: Entity? = when (floorObject) {
                is TrainWay -> TrainLineEntity(
                    name = "train" + floorObject.id,
                    headWagonDrawable = headWagonDrawable,
                    regularWagonDrawable = regularWagonDrawable,
                    tailWagonDrawable = tailWagonDrawable,
                    backgroundPaint = trainWayPaint,
                    parent = scene,
                    transform = Transform(
                        position = Vector2(floorAggregatedXOffset, 0f),
                        scale = Vector2(floorObject.width, floorObject.height)
                    )
                )

                is Wall -> WallEntity(
                    name = "wall" + floorObject.id,
                    parent = scene,
                    paint = wallPaint,
                    transform = Transform(
                        position = Vector2(floorAggregatedXOffset, 0f),
                        scale = Vector2(floorObject.width, floorObject.height)
                    )
                )

                is Platform -> PlatformEntity(
                    name = "platform" + floorObject.id,
                    parent = scene,
                    paint = platformPaint,
                    transform = Transform(
                        position = Vector2(floorAggregatedXOffset, 0f),
                        scale = Vector2(floorObject.width, floorObject.height)
                    )
                )
                    .also { entity ->
                        floorObject.navStartPoints.forEach { point ->
                            scene.addEntity(
                                NavStartPointEntity(
                                    name = point.code,
                                    drawable = navStartPointDrawable,
                                    parent = entity,
                                    position = Vector2(point.locationX, point.locationY),
                                    elevation = 2f
                                )
                            )
                        }
                        floorObject.platExits.forEach { exit ->
                            scene.addEntity(
                                PlatformExitEntity(
                                    name = exit.name,
                                    drawable = stationRectangleNinePatchDrawable,
                                    parent = entity,
                                    transform = Transform(
                                        position = Vector2(exit.locationX, exit.locationY),
                                        scale = Vector2(exit.width, exit.length)
                                    ),
                                    elevation = 2f
                                )
                            )
                        }
                    }

                else -> null
            }

            floorAggregatedXOffset += floorObject.width

            entity?.let { scene.addEntity(entity) }
        }
    }
}