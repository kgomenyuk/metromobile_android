package space.linio.app.ui.customView.stationTopDown.entity

import android.graphics.Canvas
import android.graphics.drawable.Drawable
import space.linio.base.view.customView.*
import kotlin.math.roundToInt

class NavStartPointEntity(
    name: String,
    position: Vector2,
    parent: Entity,
    elevation: Float = 1f,
    private val drawable: Drawable
) : BaseEntity(name, Transform(position, Vector2(0.12f, 0.12f)), parent, elevation),
    DrawableEntity {

    override fun draw(canvas: Canvas) {
        pixelBounds.run {
            val dimension = ((width + height) / 2f).roundToInt()

            drawable.setBounds(
                x - (dimension / 2f).roundToInt(),
                y - (dimension / 2f).roundToInt(),
                x + (dimension / 2f).roundToInt(),
                y + (dimension / 2f).roundToInt()
            )

            drawable.draw(canvas)
        }
    }
}