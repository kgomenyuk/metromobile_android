package space.linio.app.ui.activity

import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_register.*
import space.linio.app.R
import space.linio.app.presenter.RegisterPresenter
import space.linio.app.presenter.ddo.RegisterCredentials
import space.linio.app.view.RegisterView
import space.linio.base.view.BaseActivity

/**
 * Created by tetawex on 11.01.2018.
 */
class RegisterActivity : BaseActivity(), RegisterView {

    @InjectPresenter
    lateinit var registerPresenter: RegisterPresenter

    override val layoutId: Int = R.layout.activity_register

    override fun setupView() {
        //bind register button listener
        btn_register.setOnClickListener {
            registerPresenter.onRegisterActionPerformed(
                RegisterCredentials(
                    login = et_login.text.toString(),
                    email = et_email.text.toString(),
                    username = et_username.text.toString(),
                    password = et_password.text.toString(),
                    passwordConfirmation = et_password_confirm.text.toString()
                )
            )
        }
    }

    override fun preInit() {
    }

    override fun postInit() {
    }

    override fun navigateToLoginScreen() {
        finish()
    }

    override fun showMessageUserAlreadyExists() {
        Toast.makeText(this, getString(R.string.err_user_already_exists), Toast.LENGTH_SHORT).show()
    }

    override fun showMessageUserSuccessfullyRegistered() {
        Toast.makeText(this, getString(R.string.msg_user_registered), Toast.LENGTH_SHORT).show()
    }

    override fun showMessagePasswordDoesNotMatch() {
        Toast.makeText(this, getString(R.string.err_password_not_confirmed), Toast.LENGTH_SHORT)
            .show()
    }
}