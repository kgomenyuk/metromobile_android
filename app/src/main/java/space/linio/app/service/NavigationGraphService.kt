package space.linio.app.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.bumptech.glide.Glide
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import space.linio.app.App
import space.linio.app.model.api.PanoramaConfig.baseUrl
import space.linio.app.model.api.PanoramaConfig.panoramaHeight
import space.linio.app.model.api.PanoramaConfig.panoramaWidth
import space.linio.app.model.usecase.abs.RawGraphByIdUseCase
import space.linio.app.model.usecase.abs.graph.AppendGraphUseCase
import space.linio.app.model.usecase.abs.graph.ConnectGraphUseCase
import space.linio.app.model.usecase.abs.graph.NavigateGraphUseCase
import javax.inject.Inject


/**
 * Created by tetawex on 02.03.2018.
 */
class NavigationGraphService : Service() {
    @Inject
    lateinit var rawGraphByIdUseCase: RawGraphByIdUseCase
    @Inject
    lateinit var connectGraphUseCase: ConnectGraphUseCase
    @Inject
    lateinit var appendGraphUseCase: AppendGraphUseCase
    @Inject
    lateinit var navigateGraphUseCase: NavigateGraphUseCase

    private val navigationUpdateDisposable = Disposables.empty()

    override fun onCreate() {
        super.onCreate()
        App.componentManager.navigationGraphComponent.inject(this)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startid: Int): Int {
        appendNewGraphChunk(2795)

        navigateGraphUseCase.navigationUpdateEventEmitter.subscribe {
            appendNewGraphChunk(navigateGraphUseCase.getCurrentNode().id)
        }
        return Service.START_STICKY
    }

    private fun appendNewGraphChunk(nodeId: Int) {
        rawGraphByIdUseCase
            .getRawGraph(nodeId)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnSuccess { rawGraph ->
                rawGraph.nodes.forEach {
                    Glide.with(this).load(
                        baseUrl +
                                it.media[0].mediaID
                    ).downloadOnly(panoramaWidth, panoramaHeight)
                }
            }
            .observeOn(Schedulers.computation())
            .subscribe(
                { rawGraph ->
                    appendGraphUseCase.appendGraph(connectGraphUseCase.connectGraph(rawGraph))
                }, { it.printStackTrace() })
    }
}