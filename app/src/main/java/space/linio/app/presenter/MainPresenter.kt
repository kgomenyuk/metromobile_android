package space.linio.app.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

import space.linio.app.view.MainView
import space.linio.app.view.ScreenTag

/**
 * Created by Tetawex on 29.10.2017.
 */
@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {

    fun onTabSelected(tag: ScreenTag) {
        viewState.setScreen(tag)
    }

    fun onBackStackPopped() {}
}
