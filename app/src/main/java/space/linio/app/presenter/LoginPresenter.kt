package space.linio.app.presenter

import com.arellomobile.mvp.InjectViewState
import space.linio.app.App
import space.linio.app.model.usecase.abs.LoginByNamePassUseCase
import space.linio.app.model.usecase.abs.RegisterUseCase
import space.linio.app.view.LoginView
import space.linio.base.BasePresenter
import space.linio.base.utils.rxextensions.applyDefaultSchedulers
import javax.inject.Inject

@InjectViewState
class LoginPresenter : BasePresenter<LoginView>() {

    @Inject
    lateinit var loginByNamePassUseCase: LoginByNamePassUseCase
    @Inject
    lateinit var registerUseCase: RegisterUseCase

    init {
        App.componentManager.loginPresenterComponent.inject(this)
    }

    fun onLoginActionPerformed(username: String, password: String) {
        clearDisposables()
        addDisposable(loginByNamePassUseCase
            .login(username, password)
            .applyDefaultSchedulers()
            .applyProgressbarBeforeAndAfter()
            .subscribe(
                { viewState.navigateToMainScreen() }
            ) { throwable ->
                viewState.showError(throwable)
                throwable.printStackTrace()
            })
    }

    fun onRegisterActionPerformed() {
        viewState.navigateToRegisterScreen()
    }
}