package space.linio.app.presenter.ddo

import org.joda.time.DateTime

@Deprecated("")
data class DisplayNodeData(
    var parentNode: RouteNodeData,
    var routeNodeType: RouteNodeType,
    var description: String,
    var estimatedArrivalTime: DateTime
) {}