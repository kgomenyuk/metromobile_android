package space.linio.app.presenter

import com.arellomobile.mvp.InjectViewState
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import retrofit2.HttpException
import space.linio.app.App
import space.linio.app.model.usecase.abs.RegisterUseCase
import space.linio.app.presenter.ddo.RegisterCredentials
import space.linio.app.view.RegisterView
import space.linio.base.BasePresenter
import space.linio.base.utils.rxextensions.applyDefaultSchedulers
import javax.inject.Inject

@InjectViewState
class RegisterPresenter : BasePresenter<RegisterView>() {

    @Inject
    lateinit var registerUseCase: RegisterUseCase

    private var registerDisposable: Disposable = Disposables.empty()

    init {
        App.componentManager.registerPresenterComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

    }

    fun onRegisterActionPerformed(credentials: RegisterCredentials) {
        if (credentials.password != credentials.passwordConfirmation) {
            viewState.showMessagePasswordDoesNotMatch()
            return
        }

        registerDisposable.dispose()
        registerDisposable = addDisposable(registerUseCase
            .register(credentials)
            .applyDefaultSchedulers()
            .applyProgressbarBeforeAndAfter()
            .subscribe(
                {
                    viewState.showMessageUserSuccessfullyRegistered()
                    viewState.navigateToLoginScreen()
                },
                { throwable ->
                    if (throwable is HttpException &&
                        throwable.code() >= 500
                    ) {
                        viewState.showMessageUserAlreadyExists()
                    } else
                        viewState.showError(throwable)
                    throwable.printStackTrace()
                }
            )
        )
    }
}