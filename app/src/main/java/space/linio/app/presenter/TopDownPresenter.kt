package space.linio.app.presenter


import com.arellomobile.mvp.InjectViewState
import space.linio.app.App
import space.linio.app.model.usecase.abs.CurrentStationSchemaUseCase
import space.linio.app.view.TopDownView
import space.linio.base.BasePresenter
import space.linio.base.utils.rxextensions.applyDefaultSchedulers
import javax.inject.Inject

/**
 * Created by Tetawex on 30.10.2017.
 */
@InjectViewState
class TopDownPresenter : BasePresenter<TopDownView>() {
    @Inject
    lateinit var currentStationSchemaUseCase: CurrentStationSchemaUseCase

    init {
        App.componentManager.topDownPresenterComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        addDisposable(
            currentStationSchemaUseCase
                .getCurrentStationSchema()
                .applyDefaultSchedulers()
                .applyProgressbarBeforeAndAfter()
                .subscribe(
                    {
                        viewState.setStationFloorSchema(it.stations[0].floors[0])
                    },
                    {
                        viewState.showError(it)
                        it.printStackTrace()
                    })
        )
    }
}
