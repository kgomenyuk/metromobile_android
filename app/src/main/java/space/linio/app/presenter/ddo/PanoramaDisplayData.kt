package space.linio.app.presenter.ddo

/**
 * Created by tetawex on 11/27/17.
 */
data class PanoramaDisplayData(
    val imageUrl: String,
    val initialCameraAngle: Float = 0f,
    val isPanorama: Boolean = false
)