package space.linio.app.presenter.ddo

/**
 * Created by Tetawex on 08.02.2018.
 */
data class RegisterCredentials(
    val login: String,
    val username: String,
    val email: String,
    val password: String,
    val passwordConfirmation: String
)