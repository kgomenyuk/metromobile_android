package space.linio.app.presenter.ddo

/**
 * Created by tetawex on 11/27/17.
 */
@Deprecated("...")
enum class RouteNodeType {
    TRAIN, PLATFORM, STAIRS, ESCALATOR
}