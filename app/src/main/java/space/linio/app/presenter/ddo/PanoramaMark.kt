package space.linio.app.presenter.ddo

/**
 * Created by tetawex on 11/27/17.
 */
data class PanoramaMark(
    var x: Float,
    var y: Float,
    var highlighted: Boolean,
    var nodeType: RouteNodeType,
    var parentNode: RouteNodeData,
    var description: String
)