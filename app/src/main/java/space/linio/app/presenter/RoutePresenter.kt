package space.linio.app.presenter

import com.arellomobile.mvp.InjectViewState
import io.reactivex.rxkotlin.Singles
import space.linio.app.App
import space.linio.app.model.usecase.abs.NodeByQueryUseCase
import space.linio.app.model.usecase.abs.RouteByNodesUseCase
import space.linio.app.presenter.ddo.DisplayNodeData
import space.linio.app.view.RouteView
import space.linio.base.BasePresenter
import space.linio.base.utils.RxUtils.composeDelayBefore
import javax.inject.Inject

@InjectViewState
class RoutePresenter : BasePresenter<RouteView>() {
    @Inject
    lateinit var nodeByQueryUseCase: NodeByQueryUseCase
    @Inject
    lateinit var routeByNodesUseCase: RouteByNodesUseCase

    private var toQuery: String = ""
    private var fromQuery: String = ""

    init {
        App.componentManager.mapPresenterComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.displayEmptyRouteHint()
    }

    fun onRouteItemPressed(routeItem: DisplayNodeData) {

    }

    fun onFromQueryChanged(query: String) {
        toQuery = query
        onQueryChanged()
    }

    fun onToQueryChanged(query: String) {
        fromQuery = query
        onQueryChanged()
    }

    private fun onQueryChanged() {
        clearDisposables()
        if (toQuery.isNotBlank() && fromQuery.isNotBlank()) {
            viewState.hideEmptyRouteHint()
            addDisposable(Singles.zip(
                nodeByQueryUseCase.getNode(fromQuery),
                nodeByQueryUseCase.getNode(toQuery),
                { n1, n2 -> Pair(n1, n2) })
                .flatMap { pair ->
                    routeByNodesUseCase
                        .getRoute(fromNodeId = pair.first.nodeId, toNodeId = pair.second.nodeId)
                }
                .compose(composeDelayBefore())
                .compose(composeSingleProgressbar())
                .subscribe(
                    { list -> viewState.setNodeList(list.map { item -> item.displayNodeData }) },
                    { throwable -> viewState.showError(throwable) })
            )
        } else
            viewState.displayEmptyRouteHint()
    }
}