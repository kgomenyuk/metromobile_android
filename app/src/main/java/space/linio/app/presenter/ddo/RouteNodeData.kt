package space.linio.app.presenter.ddo

import org.joda.time.DateTime

/**
 * Created by tetawex on 11/27/17.
 */
@Deprecated("...")
data class RouteNodeData(
    var imageUrl: String,
    var adjacentNodesList: List<RouteNodeData>,
    var panoramaMarkList: List<PanoramaMark>,
    var routeNodeType: RouteNodeType,
    var nodeId: Int,
    var description: String,
    var estimatedArrivalTime: DateTime
) {
    var panoramaData: PanoramaDisplayData
        get() = PanoramaDisplayData(
            imageUrl = imageUrl,
            initialCameraAngle = 0f,
            isPanorama = true
        )
        set(value) {}
    var displayNodeData: DisplayNodeData
        get() = DisplayNodeData(
            description = description,
            estimatedArrivalTime = estimatedArrivalTime,
            parentNode = this, routeNodeType = routeNodeType
        )
        set(value) {}
}