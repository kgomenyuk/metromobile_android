package space.linio.app.presenter


import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import space.linio.app.App
import space.linio.app.model.dto.data.graph.connected.GraphBackStackIsEmptyException
import space.linio.app.model.usecase.abs.graph.NavigateGraphUseCase
import space.linio.app.view.PanoramaView
import space.linio.base.BasePresenter
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Tetawex on 30.10.2017.
 */
@InjectViewState
class PanoramaPresenter : BasePresenter<PanoramaView>() {
    @Inject
    lateinit var navigateGraphUseCase: NavigateGraphUseCase

    init {
        App.componentManager.navigatorPresenterComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        onNavigationPerformed(2795)
    }

    fun onNavigationPerformed(nodeId: Int) {
        addDisposable(navigateGraphUseCase.moveToNodeById(nodeId)
            .subscribeOn(Schedulers.computation())
            .retryWhen { errors -> errors.delay(2, TimeUnit.SECONDS) }
            .observeOn(AndroidSchedulers.mainThread())
            .compose(composeCompletableProgressbar())
            .subscribe(
                {
                    viewState.setDisplayNode(navigateGraphUseCase.getCurrentNode())
                    viewState.hideProgressbar()
                },
                {
                    viewState.showError(it)
                    viewState.hideProgressbar()
                }))
    }

    fun onGoBackPerformed() {
        addDisposable(navigateGraphUseCase.goBack()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .compose(composeCompletableProgressbar())
            .subscribe(
                { viewState.setDisplayNode(navigateGraphUseCase.getCurrentNode()) },
                { throwable ->
                    if (throwable is GraphBackStackIsEmptyException) {
                        viewState.showCantGoBackHint()
                    } else {
                        throwable.printStackTrace()
                        viewState.showError(throwable)
                    }
                }
            ))
    }
}
