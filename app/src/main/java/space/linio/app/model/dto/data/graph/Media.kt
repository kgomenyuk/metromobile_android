package space.linio.app.model.dto.data.graph

import com.squareup.moshi.Json
import space.linio.app.model.api.PanoramaConfig

data class Media(

    @Json(name = "MediaID")
    var mediaID: Int,

    @Json(name = "MediaType")
    val mediaType: String = PanoramaConfig.imageTypePhoto
) {
}
