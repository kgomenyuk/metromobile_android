package space.linio.app.model.usecase.impl

import io.reactivex.Single
import space.linio.app.model.dto.data.stationschema.StationSchemaResponse
import space.linio.app.model.repository.DataRepository
import space.linio.app.model.usecase.abs.CurrentStationSchemaUseCase

class CurrentStationSchemaInteractor(private val dataRepository: DataRepository) :
    CurrentStationSchemaUseCase {
    override fun getCurrentStationSchema(): Single<StationSchemaResponse> {
        return dataRepository.getCurrentStationSchema()
    }
}