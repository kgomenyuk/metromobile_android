package space.linio.app.model.usecase.impl

import io.reactivex.Single
import space.linio.app.model.repository.DataRepository
import space.linio.app.model.usecase.abs.NodeByQueryUseCase
import space.linio.app.presenter.ddo.RouteNodeData
import space.linio.app.util.NodeStubCreator
import javax.inject.Inject

class NodeByQueryInteractor
@Inject constructor(private val dataRepository: DataRepository) : NodeByQueryUseCase {
    override fun getNode(query: String): Single<RouteNodeData> {
        //TODO Replace stub with repository data
        return Single.just(NodeStubCreator.createNodeStub())
    }
}