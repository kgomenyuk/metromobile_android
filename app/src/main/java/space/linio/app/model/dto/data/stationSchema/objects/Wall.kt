package space.linio.app.model.dto.data.stationschema.objects

import com.squareup.moshi.Json
import space.linio.app.model.dto.data.stationschema.RIType

class Wall(
    @Json(name = "WidthAbs")
    val widthAbs: Int = 0,
    @Json(name = "FloorNumber")
    val floorNumber: Int = 0,
    override val height: Float = 1f,
    @Json(name = "LengthAbs")
    val lengthAbs: String = "",
    @Json(name = "Visible")
    override val visible: Boolean = true,
    @Json(name = "StatusID")
    val statusID: String = "",
    @Json(name = "OrderNumber")
    override val orderNumber: Int = 0,
    @Json(name = "ID")
    val id: Int = 0,
    @Json(name = "RIType")
    override val rIType: RIType = RIType.WALL,
    @Json(name = "Code")
    override val code: String? = "",
    @Json(name = "Width")
    override val width: Float = 0f
) : GenericFloorObject()