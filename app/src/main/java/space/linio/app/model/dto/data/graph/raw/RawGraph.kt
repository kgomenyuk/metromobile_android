package space.linio.app.model.dto.data.graph.raw

import com.squareup.moshi.Json

data class RawGraph(

    @Json(name = "Nodes")
    val nodes: MutableList<RawNode>,
    @Json(name = "Edges")
    val edges: MutableList<RawEdge>
) {

}
