package space.linio.app.model.usecase.abs

import io.reactivex.Single
import space.linio.app.model.dto.data.graph.raw.RawGraph

/**
 * Created by tetawex on 02.03.2018.
 */
interface RawGraphByIdUseCase {
    fun getRawGraph(nodeId: Int): Single<RawGraph>
}