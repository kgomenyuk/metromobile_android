package space.linio.app.model.dto.data.stationschema

import com.squareup.moshi.Json

data class StationsItem(
    @Json(name = "GPSLatitude")
    val gPSLatitude: Double = 0.0,
    @Json(name = "WidthAbs")
    val widthAbs: Int = 0,
    @Json(name = "CityID")
    val cityID: Int = 0,
    @Json(name = "NameEN")
    val nameEN: String = "",
    @Json(name = "LengthAbs")
    val lengthAbs: Int = 0,
    @Json(name = "Visible")
    val visible: Boolean = false,
    @Json(name = "StatusID")
    val statusID: String = "",
    @Json(name = "North")
    val north: Int = 0,
    @Json(name = "ID")
    val id: Int = 0,
    @Json(name = "GPSLongitude")
    val gPSLongitude: Double = 0.0,
    @Json(name = "Floors")
    val floors: List<Floor>,
    @Json(name = "Name")
    val name: String = ""
)