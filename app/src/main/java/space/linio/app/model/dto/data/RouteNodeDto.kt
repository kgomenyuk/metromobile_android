package space.linio.app.model.dto.data

import com.squareup.moshi.Json
import space.linio.app.presenter.ddo.PanoramaMark

data class RouteNodeDto(
    @Json(name = "description") var description: String = "",
    @Json(name = "node_id") var nodeId: Int,
    @Json(name = "image_url") var imageUrl: String,
    @Json(name = "adjacent_nodes") var adjacentNodesConnectionsList: List<NodeConnection>,
    @Json(name = "mark_list") var panoramaMarkList: List<PanoramaMark>,
    @Json(name = "node_type") var routeNodeType: Int
)