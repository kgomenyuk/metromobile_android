package space.linio.app.model.dto.data.graph

import com.squareup.moshi.Json
import space.linio.app.model.dto.data.graph.raw.RawGraph

data class NavigationGraphResponse(

    @Json(name = "Graph")
    var graph: RawGraph,
    @Json(name = "Result")
    var result: String
) {

}
