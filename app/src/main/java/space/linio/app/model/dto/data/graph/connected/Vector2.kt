package space.linio.app.model.dto.data.graph.connected

/**
 * Created by Tetawex on 12.04.2018.
 */
data class Vector2(var x: Float, val y: Float) {
}