package space.linio.app.model.repository

import io.reactivex.Single
import space.linio.app.model.dto.data.stationschema.StationSchemaResponse
import space.linio.app.model.dto.response.RegisterResponse
import space.linio.app.presenter.ddo.RegisterCredentials

/**
 * Created by Tetawex on 31.10.2017.
 */

interface DataRepository {
    fun getToken(username: String, password: String): Single<String>
    fun registerUser(credentials: RegisterCredentials): Single<RegisterResponse>
    fun getCurrentStationSchema(): Single<StationSchemaResponse>//TODO move outta here
}
