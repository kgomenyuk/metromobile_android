package space.linio.app.model.usecase.impl.graph

import space.linio.app.model.dto.data.graph.connected.Edge
import space.linio.app.model.dto.data.graph.connected.Graph
import space.linio.app.model.repository.ConnectedGraphRepository
import space.linio.app.model.usecase.abs.graph.AppendGraphUseCase
import java.util.*

class AppendGraphInteractor(private val repository: ConnectedGraphRepository) : AppendGraphUseCase {
    override fun appendGraph(new: Graph) {
        val nodes = repository.graph.nodes

        new.nodes.forEach {
            val newId = it.key
            val newNode = it.value

            if (!nodes.containsKey(newId)) {
                //for each node that does not exist in the base graph add it and connect edges
                nodes[newId] = newNode
                newNode.edges.forEach {
                    if (nodes.containsKey(it.toId)) {
                        it.toNode = nodes[it.toId]
                    }
                    if (nodes.containsKey(it.fromId)) {
                        it.fromNode = nodes[it.fromId]
                    }
                }
            } else {
                //if exists, replace while merging edge lists
                val oldNode = nodes[newId]!!
                val newEdgeSet = TreeSet<Edge>()
                oldNode.edges.forEach { oldNodeEdge ->
                    newEdgeSet.add(oldNodeEdge)

                    newNode.edges.forEach { newNodeEdge ->
                        if (oldNodeEdge == newNodeEdge) {
                            //merge references
                            if (oldNodeEdge.fromNode == null && newNodeEdge.fromNode != null) {
                                oldNodeEdge.fromNode = newNodeEdge.fromNode
                            }
                            if (oldNodeEdge.toNode == null && newNodeEdge.toNode != null) {
                                oldNodeEdge.toNode = newNodeEdge.toNode
                            }
                        }
                    }
                }
                newEdgeSet.addAll(newNode.edges)
                oldNode.edges = newEdgeSet.toMutableList()
            }
        }
    }
}