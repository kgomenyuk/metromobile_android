package space.linio.app.model.usecase.abs

import io.reactivex.Completable
import io.reactivex.Single
import space.linio.app.model.entity.LoginCredentials

/**
 * Created by tetawex on 25.07.2018.
 */
interface LoginCredentialsUseCase {
    fun getLoginCredentials(): Single<LoginCredentials>
    fun setLoginCredentials(loginCredentials: LoginCredentials): Completable
}