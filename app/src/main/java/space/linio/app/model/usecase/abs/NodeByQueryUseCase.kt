package space.linio.app.model.usecase.abs

import io.reactivex.Single
import space.linio.app.presenter.ddo.RouteNodeData

/**
 * Created by Tetawex.
 */

interface NodeByQueryUseCase {
    fun getNode(query: String): Single<RouteNodeData>
}