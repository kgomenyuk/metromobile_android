package space.linio.app.model.repository

import io.reactivex.Completable
import io.reactivex.Single
import space.linio.app.model.api.ApiInterface
import space.linio.app.model.dto.data.graph.raw.RawGraph

class RestCachingRawGraphRepository(
    private val apiInterface: ApiInterface,
    private val tokenProvider: TokenProvider
) : RawGraphRepository {
    override fun getRawGraph(nodeId: Int): Single<RawGraph> {
        return apiInterface
            .getRawGraph(token = tokenProvider.token, langId = "ru", nodeId = nodeId, depth = 50)
            .map { it.graph }
    }

    override fun checkForUpdates(): Completable {
        return Completable.complete()
    }
}