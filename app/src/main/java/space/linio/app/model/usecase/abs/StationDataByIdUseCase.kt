package space.linio.app.model.usecase.abs

import io.reactivex.Single
import space.linio.app.model.dto.data.StationData

/**
 * Created by Tetawex on 31.10.2017.
 */

interface StationDataByIdUseCase {
    fun getStationData(id: Int): Single<StationData>
}
