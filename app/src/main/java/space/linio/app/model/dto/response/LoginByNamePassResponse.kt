package space.linio.app.model.dto.response

import com.squareup.moshi.Json


data class LoginByNamePassResponse(
    @Json(name = "is_authenticated") val authenticated: Boolean,
    @Json(name = "token") val token: String
)