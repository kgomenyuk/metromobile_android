package space.linio.app.model.dto.data.graph.raw

import com.squareup.moshi.Json
import space.linio.app.model.dto.data.graph.Coords

data class RawEdge(

    @Json(name = "Target")
    val target: Int = 0,
    @Json(name = "Source")
    val source: Int = 0,
    @Json(name = "Coords")
    val coords: Coords = Coords(0f, 0f),
    @Json(name = "Direction")
    val direction: String = "",
    @Json(name = "IsReverse")
    val isReverse: Boolean = false,
    @Json(name = "Step")
    val step: Any? = Any()
) {

}
