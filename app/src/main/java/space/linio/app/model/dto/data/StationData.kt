package space.linio.app.model.dto.data

import com.squareup.moshi.Json

/**
 * Created by Tetawex on 29.10.2017.
 */

data class StationData(
    @Json(name = "title") var title: String = "",
    var stationId: Int = 1,
    val northAngle: Float = 0f,
    var arrowList: List<Arrow>
)
