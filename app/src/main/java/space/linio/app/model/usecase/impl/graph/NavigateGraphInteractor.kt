package space.linio.app.model.usecase.impl.graph

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import space.linio.app.model.dto.data.graph.connected.GraphBackStackIsEmptyException
import space.linio.app.model.dto.data.graph.connected.Node
import space.linio.app.model.repository.ConnectedGraphRepository
import space.linio.app.model.usecase.abs.graph.NavigateGraphUseCase
import space.linio.app.model.usecase.abs.graph.NavigationUpdateEvent

class NavigateGraphInteractor(private val repository: ConnectedGraphRepository) :
    NavigateGraphUseCase {

    private val mNavigationUpdatesEventEmitter = PublishSubject.create<NavigationUpdateEvent>()
    override val navigationUpdateEventEmitter: PublishSubject<NavigationUpdateEvent> =
        mNavigationUpdatesEventEmitter

    override fun goBack(): Completable {
        return Completable.fromRunnable {
            val graph = repository.graph

            //if back stack is empty throw exception
            if (!graph.nodeBackStack.empty()) {
                //backup currentNode
                val prevNode = getCurrentNode()
                graph.currentNode = graph.nodeBackStack.pop()

                //emit event
                mNavigationUpdatesEventEmitter.onNext(
                    NavigationUpdateEvent(
                        fromNode = prevNode,
                        toNode = getCurrentNode()
                    )
                )

            } else throw GraphBackStackIsEmptyException()
        }
    }

    override fun getNodeById(id: Int): Single<Node> {
        return Single.defer { Single.just(repository.graph.nodes[id]!!) }
    }

    override fun getCurrentNode(): Node {
        return repository.graph.currentNode!!
    }

    override fun moveToNodeById(id: Int): Completable {
        return Completable.fromRunnable {
            if (repository.graph.nodes.containsKey(id)) {
                //backup current node
                val currentNodeBackup = repository.graph.currentNode

                //might throw an exception which aborts the whole transaction
                repository.graph.currentNode = repository.graph.nodes[id]!!// [1]

                //avoid adding null nodes to backstack
                if (currentNodeBackup != null) {
                    //current node is added to backstack after map retrieval at [1]
                    // to avoid unwanted modifications in case the map retrieval fails
                    repository.graph.nodeBackStack.push(currentNodeBackup)

                    //Emit an update on successful navigation
                    mNavigationUpdatesEventEmitter.onNext(
                        NavigationUpdateEvent(
                            fromNode = currentNodeBackup,
                            toNode = getCurrentNode()
                        )
                    )
                }

            }
        }
    }
}