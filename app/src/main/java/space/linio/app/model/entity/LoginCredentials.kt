package space.linio.app.model.entity

/**
 * Created by tetawex on 25.07.2018.
 */
data class LoginCredentials(val login: String, val password: String)