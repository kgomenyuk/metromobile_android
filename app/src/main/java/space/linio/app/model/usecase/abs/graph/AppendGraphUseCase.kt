package space.linio.app.model.usecase.abs.graph

import space.linio.app.model.dto.data.graph.connected.Graph

/**
 * Created by Tetawex on 14.04.2018.
 */
interface AppendGraphUseCase {
    fun appendGraph(new: Graph)
}