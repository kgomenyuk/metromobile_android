package space.linio.app.model.usecase.abs

import io.reactivex.Completable

interface LoginByNamePassUseCase {
    fun login(username: String, password: String): Completable
}
