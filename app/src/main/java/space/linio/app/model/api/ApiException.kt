package space.linio.app.model.api

import java.io.IOException

/**
 * Created by Tetawex on 29.10.2017.
 */

class ApiException(val errorType: ErrorType) : IOException() {

    enum class ErrorType {
        CLIENT, SERVER, NOT_AUTHORISED
    }
}

