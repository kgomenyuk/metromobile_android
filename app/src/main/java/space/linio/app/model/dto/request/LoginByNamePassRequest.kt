package space.linio.app.model.dto.request

import com.squareup.moshi.Json

data class LoginByNamePassRequest(
    @Json(name = "login") val username: String,
    @Json(name = "password") val password: String
) {
}