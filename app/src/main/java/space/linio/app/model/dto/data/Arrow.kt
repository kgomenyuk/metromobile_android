package space.linio.app.model.dto.data

/**
 * Created by Tetawex on 29.10.2017.
 */

data class Arrow(var angle: Float = 0f, var title: String = "")
