package space.linio.app.model.repository

import io.reactivex.Completable
import io.reactivex.Single
import space.linio.app.model.dto.data.graph.raw.RawGraph

/**
 * Created by tetawex on 02.03.2018.
 */
interface RawGraphRepository {
    fun getRawGraph(nodeId: Int): Single<RawGraph>
    fun checkForUpdates(): Completable
}