package space.linio.app.model.repository

/**
 * Created by Tetawex on 13.04.2018.
 */
interface TokenProvider {
    val token: String
}