package space.linio.app.model.dto.data.graph.connected

/**
 * Created by Tetawex on 12.04.2018.
 */
data class Edge(
    var fromNode: Node?,
    var fromId: Int,
    var toNode: Node?,
    var toId: Int,
    val displayPosition: Vector2
) : Comparable<Edge> {
    override fun compareTo(other: Edge): Int {
        return if (toId == other.toId && fromId == other.fromId) 0
        else if (toId + fromId >= other.toId + other.fromId) 1
        else -1
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Edge

        if (toId == other.toId && fromId == other.fromId) return false

        return true
    }
}
