package space.linio.app.model.dto.data.stationschema

import com.squareup.moshi.Json

data class StationSchemaResponse(
    @Json(name = "OuterExits")
    val outerExits: List<OuterExitsItem>,
    @Json(name = "Stations")
    val stations: List<StationsItem>,
    @Json(name = "POI")
    val poi: List<POIItem>,
    @Json(name = "Result")
    val result: String = ""
)