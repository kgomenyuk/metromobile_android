package space.linio.app.model.usecase.impl

import io.reactivex.Single
import space.linio.app.model.dto.data.graph.raw.RawGraph
import space.linio.app.model.repository.RawGraphRepository
import space.linio.app.model.usecase.abs.RawGraphByIdUseCase

class RawGraphByIdInteractor(private val graphRepository: RawGraphRepository) :
    RawGraphByIdUseCase {
    override fun getRawGraph(nodeId: Int): Single<RawGraph> {
        return graphRepository.getRawGraph(nodeId)
    }
}