package space.linio.app.model.usecase.impl

import io.reactivex.Completable
import space.linio.app.model.repository.DataRepository
import space.linio.app.model.usecase.abs.RegisterUseCase
import space.linio.app.presenter.ddo.RegisterCredentials

class RegisterInteractor(private val dataRepository: DataRepository) : RegisterUseCase {
    override fun register(credentials: RegisterCredentials): Completable {
        return dataRepository.registerUser(credentials).toCompletable()
    }
}