package space.linio.app.model.api

import io.reactivex.Single
import retrofit2.http.*
import space.linio.app.model.dto.data.graph.NavigationGraphResponse
import space.linio.app.model.dto.data.stationschema.StationSchemaResponse
import space.linio.app.model.dto.request.LoginByNamePassRequest
import space.linio.app.model.dto.request.RegisterRequest
import space.linio.app.model.dto.response.LoginByNamePassResponse
import space.linio.app.model.dto.response.RegisterResponse

interface ApiInterface {

    @POST("api/loginAuth")
    fun getToken(@Body request: LoginByNamePassRequest): Single<LoginByNamePassResponse>

    @POST("api/user/register")
    fun registerUser(@Body request: RegisterRequest): Single<RegisterResponse>

    @GET("https://linio.space/navigation/get_nav_graph/{langId}/{nodeId}/{depth}")
    fun getRawGraph(
        @Path("langId") langId: String,
        @Path("nodeId") nodeId: Int,
        @Path("depth") depth: Int,
        @Query("token") token: String
    ): Single<NavigationGraphResponse>

    @GET("api/station_items_hier/{langId}/{stationId}")
    fun getStationSchema(
        @Path("langId") langId: String,
        @Path("stationId") stationId: Int
    ): Single<StationSchemaResponse>
}
