package space.linio.app.model.dto.data.stationschema

import com.squareup.moshi.Json

enum class RIType(val code: String) {
    @Json(name = "WAY")
    TRAIN_WAY(code = "WAL"),
    @Json(name = "WAL")
    WALL(code = "WAL"),
    @Json(name = "IEX")
    INNER_EXIT(code = "IEX"),
    @Json(name = "OEX")
    OUTER_EXIT(code = "OEX"),
    @Json(name = "FLR")
    FLOOR(code = "OEX"),
    @Json(name = "PLT")
    PLATFORM(code = "PLT"),
    @Json(name = "POI")
    POINT_OF_INTEREST(code = "POI"),
    UNDEFINED(code = "UDF")
}