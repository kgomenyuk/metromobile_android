package space.linio.app.model.dto.data.stationschema

import com.squareup.moshi.Json

data class NavStartPoint(
    @Json(name = "StartStepID") val startStepID: String? = "",
    @Json(name = "NavigationType") val navigationType: String? = "",
    @Json(name = "ID") val id: Int? = 0,
    @Json(name = "Code") val code: String = "",
    @Json(name = "LocationX") val locationX: Float = 0.5f,
    @Json(name = "LocationY") val locationY: Float = 0.5f
)