package space.linio.app.model.dto.response

import com.squareup.moshi.Json

/**
 * Created by Tetawex on 08.02.2018.
 */

data class RegisterResponse(
    @Json(name = "userID")
    val userID: Int
)
