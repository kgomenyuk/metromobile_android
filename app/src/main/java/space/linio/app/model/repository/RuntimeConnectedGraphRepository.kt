package space.linio.app.model.repository

import space.linio.app.model.dto.data.graph.connected.Graph

class RuntimeConnectedGraphRepository : ConnectedGraphRepository {
    private var mGraph: Graph = Graph()
    override var graph: Graph
        get() = mGraph
        set(value) {
            mGraph = value
        }
}