package space.linio.app.model.dto.data.stationschema

import com.squareup.moshi.Json
import space.linio.app.model.dto.data.stationschema.objects.GenericFloorObject

data class Floor(
    @Json(name = "WidthAbs")
    val widthAbs: Int = 0,
    @Json(name = "FloorNumber")
    val floorNumber: Int = 0,
    @Json(name = "LengthAbs")
    val lengthAbs: Int = 0,
    @Json(name = "Visible")
    val visible: Boolean = false,
    @Json(name = "Objects")
    val objects: List<GenericFloorObject>,
    @Json(name = "StatusID")
    val statusID: String = "",
    @Json(name = "ID")
    val id: Int = 0,
    @Json(name = "RIType")
    val rIType: RIType = RIType.FLOOR,
    @Json(name = "Code")
    val code: String? = ""
)