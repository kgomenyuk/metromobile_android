package space.linio.app.model.dto.data.graph.connected

import java.util.*

/**
 * Created by Tetawex on 12.04.2018.
 */
class Graph {
    val nodes = HashMap<Int, Node>(50)
    val edges = LinkedList<Edge>()

    var currentNode: Node? = null
    val nodeBackStack = Stack<Node>()
}