package space.linio.app.model.dto.data

import com.squareup.moshi.Json

data class NodeConnection(
    @Json(name = "node_1_id") var nodeId1: Int,
    @Json(name = "node_2_id") var nodeId2: Int,
    @Json(name = "travel_time") var travelTime: Long
)