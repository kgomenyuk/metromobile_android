package space.linio.app.model.dto.response

import space.linio.app.model.dto.data.StationData

/**
 * Created by Tetawex on 29.10.2017.
 */

class StationDataResponse(val data: StationData)
