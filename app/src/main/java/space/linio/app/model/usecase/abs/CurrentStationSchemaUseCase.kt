package space.linio.app.model.usecase.abs

import io.reactivex.Single
import space.linio.app.model.dto.data.stationschema.StationSchemaResponse

interface CurrentStationSchemaUseCase {
    fun getCurrentStationSchema(): Single<StationSchemaResponse>
}