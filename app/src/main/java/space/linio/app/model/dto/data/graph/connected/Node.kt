package space.linio.app.model.dto.data.graph.connected

import space.linio.app.presenter.ddo.PanoramaDisplayData

/**
 * Created by Tetawex on 12.04.2018.
 */
data class Node(
    val id: Int,
    var edges: MutableList<Edge>,
    val displayData: PanoramaDisplayData
) {
}