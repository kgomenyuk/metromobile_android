package space.linio.app.model.dto.request

import com.squareup.moshi.Json

/**
 * Created by Tetawex on 08.02.2018.
 */

data class RegisterRequest(
    @Json(name = "Login")
    val login: String,
    @Json(name = "Username")
    val username: String,
    @Json(name = "Password")
    val password: String,
    @Json(name = "Email")
    val email: String,
    @Json(name = "Roles")
    val roles: List<String>
)

