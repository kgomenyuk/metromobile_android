package space.linio.app.model.dto.data.graph

import com.squareup.moshi.Json

data class Coords(

    @Json(name = "XCoord")
    var xCoord: Float = 0f,
    @Json(name = "YCoord")
    var yCoord: Float = 0f
) {

}
