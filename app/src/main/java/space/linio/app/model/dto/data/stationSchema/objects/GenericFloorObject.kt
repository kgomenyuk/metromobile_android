package space.linio.app.model.dto.data.stationschema.objects

import com.squareup.moshi.Json
import space.linio.app.model.dto.data.stationschema.RIType

open class GenericFloorObject(
    @Json(name = "Length")
    open val height: Float = 1f,
    @Json(name = "Visible")
    open val visible: Boolean = true,
    @Json(name = "OrderNumber")
    open val orderNumber: Int = 0,
    @Json(name = "RIType")
    open val rIType: RIType = RIType.UNDEFINED,
    @Json(name = "Code")
    open val code: String? = "",
    @Json(name = "Width")
    open val width: Float = 0f
)