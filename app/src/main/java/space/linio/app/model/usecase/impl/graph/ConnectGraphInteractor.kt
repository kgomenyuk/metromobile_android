package space.linio.app.model.usecase.impl.graph

import space.linio.app.model.api.PanoramaConfig
import space.linio.app.model.dto.data.graph.connected.Edge
import space.linio.app.model.dto.data.graph.connected.Graph
import space.linio.app.model.dto.data.graph.connected.Node
import space.linio.app.model.dto.data.graph.connected.Vector2
import space.linio.app.model.dto.data.graph.raw.RawEdge
import space.linio.app.model.dto.data.graph.raw.RawGraph
import space.linio.app.model.dto.data.graph.raw.RawNode
import space.linio.app.model.usecase.abs.graph.ConnectGraphUseCase
import space.linio.app.presenter.ddo.PanoramaDisplayData
import java.util.*

class ConnectGraphInteractor : ConnectGraphUseCase {
    override fun connectGraph(raw: RawGraph): Graph {
        val graph = Graph()

        //Convert each node and put it into hash map
        raw.nodes.forEach {
            graph.nodes[it.nodeID] = convertNode(it)
        }
        //Convert edges and connect the graph using node ids
        raw.edges.forEach {
            graph.edges.add(convertEdge(it, graph.nodes))
        }

        return graph
    }

    private fun convertNode(initialNode: RawNode): Node {
        return Node(
            initialNode.nodeID,
            LinkedList(),
            PanoramaDisplayData(
                imageUrl = PanoramaConfig.baseUrl + initialNode.media[0].mediaID,
                isPanorama = when (initialNode.media[0].mediaType) {
                    PanoramaConfig.imageTypePanorama -> true
                    else -> false
                }
            )
        )
    }

    private fun convertEdge(initialEdge: RawEdge, nodes: HashMap<Int, Node>): Edge {
        val fromNode = nodes[initialEdge.source]
        val toNode = nodes[initialEdge.target]

        val edge = Edge(
            fromNode = fromNode,
            fromId = initialEdge.source,
            toNode = toNode,
            toId = initialEdge.target,
            displayPosition = Vector2(initialEdge.coords.xCoord, initialEdge.coords.yCoord)
        )

        //Add edge to node's list of edges (only from)
        fromNode?.edges?.add(edge)

        return edge;
    }
}