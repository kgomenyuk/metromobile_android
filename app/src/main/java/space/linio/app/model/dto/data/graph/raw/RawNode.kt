package space.linio.app.model.dto.data.graph.raw

import com.squareup.moshi.Json
import space.linio.app.model.api.PanoramaConfig
import space.linio.app.model.dto.data.graph.Media

data class RawNode(
    @Json(name = "NodeID")
    val nodeID: Int = 0,
    @Json(name = "Media")
    val media: List<Media> = listOf(Media(0, PanoramaConfig.imageTypePhoto)),
    @Json(name = "StepType")
    val stepType: Any? = null,
    @Json(name = "StepSide")
    val stepSide: Any? = null,
    @Json(name = "Objects")
    val objects: List<Any>? = null
)
