package space.linio.app.model.usecase.abs

import io.reactivex.Completable
import space.linio.app.presenter.ddo.RegisterCredentials

interface RegisterUseCase {
    fun register(credentials: RegisterCredentials): Completable
}