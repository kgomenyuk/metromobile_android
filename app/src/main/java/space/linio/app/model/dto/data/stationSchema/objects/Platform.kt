package space.linio.app.model.dto.data.stationschema.objects

import com.squareup.moshi.Json
import space.linio.app.model.dto.data.stationschema.NavStartPoint
import space.linio.app.model.dto.data.stationschema.RIType

class Platform(
    @Json(name = "WidthAbs")
    val widthAbs: Int = 0,
    @Json(name = "FloorNumber")
    val floorNumber: Int = 0,
    @Json(name = "NavStartPoints")
    val navStartPoints: List<NavStartPoint>,
    @Json(name = "LengthAbs")
    val lengthAbs: Int = 0,
    @Json(name = "OrderNumber")
    override val orderNumber: Int = 0,
    @Json(name = "RIType")
    override val rIType: RIType = RIType.PLATFORM,
    @Json(name = "Code")
    override val code: String? = "",
    @Json(name = "PlatExits")
    val platExits: List<PlatformExit>,
    @Json(name = "Length")
    override val height: Float = 1f,
    @Json(name = "Visible")
    override val visible: Boolean = false,
    @Json(name = "StatusID")
    val statusID: String = "",
    @Json(name = "ID")
    val id: Int = 0,
    @Json(name = "Width")
    override val width: Float = 0f
) : GenericFloorObject()