package space.linio.app.model.repository

import android.content.SharedPreferences

/**
 * Created by tetawex.
 */
class PreferencesWrapper(val preferences: SharedPreferences) : TokenProvider {
    val AUTH_TOKEN_CODE = "authToken"
    val LAST_USERNAME_CODE = "lastUsername"
    var currentUserAuthToken: String
        get() = getAuthTokenForUser(getLastUsername())
        set(value) {
            preferences.edit().putString(AUTH_TOKEN_CODE + getLastUsername(), value).apply()
        }

    fun getAuthTokenForUser(username: String): String {
        return preferences.getString(AUTH_TOKEN_CODE + username, "")
    }

    fun getLastUsername(): String {
        return preferences.getString(LAST_USERNAME_CODE, "")
    }

    fun setLastUsername(username: String) {
        preferences.edit().putString(LAST_USERNAME_CODE, "").apply()
    }

    fun login(username: String, token: String) {
        val editor = preferences.edit().putString(LAST_USERNAME_CODE, username)
        editor.putString(AUTH_TOKEN_CODE + username, token)
        editor.apply()
    }

    fun logoutClear() {
        val editor = preferences.edit()
        editor.remove(LAST_USERNAME_CODE)
        editor.remove(AUTH_TOKEN_CODE + LAST_USERNAME_CODE).apply()
    }

    fun tokenExpiredClear() {
        preferences.edit().remove(AUTH_TOKEN_CODE + LAST_USERNAME_CODE).apply()
    }

    override val token = currentUserAuthToken
}