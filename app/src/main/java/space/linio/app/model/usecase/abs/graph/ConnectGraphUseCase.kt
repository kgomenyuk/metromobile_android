package space.linio.app.model.usecase.abs.graph

import space.linio.app.model.dto.data.graph.connected.Graph
import space.linio.app.model.dto.data.graph.raw.RawGraph

/**
 * Created by Tetawex on 12.04.2018.
 */
interface ConnectGraphUseCase {
    fun connectGraph(raw: RawGraph): Graph
}