package space.linio.app.model.dto.data.stationschema.objects

import space.linio.app.model.dto.data.stationschema.RIType

@Deprecated("Replaced with a class")
interface FloorObject {
    val rIType: RIType
    val width: Float
    val height: Float
    val visible: Boolean
    val orderNumber: Int
    val code: String
}