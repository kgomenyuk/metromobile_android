package space.linio.app.model.dto.data.stationschema.objects

import com.squareup.moshi.Json
import space.linio.app.model.dto.data.stationschema.RIType
import java.util.*

data class PlatformExit(
    @Json(name = "WidthAbs")
    val widthAbs: Int = 0,
    @Json(name = "SideAccess")
    val sideAccess: String = "",
    @Json(name = "AccessIn")
    val accessIn: Boolean = false,
    @Json(name = "LengthAbs")
    val lengthAbs: Int = 0,
    @Json(name = "AccessOut")
    val accessOut: Boolean = false,
    @Json(name = "RIType")
    override val rIType: RIType = RIType.PLATFORM,
    @Json(name = "ExitType")
    val exitType: String = "",
    @Json(name = "Code")
    override val code: String? = "",
    @Json(name = "Name")
    val name: String = "",
    @Json(name = "LocationY")
    val locationY: Float = 0.0f,
    @Json(name = "Length")
    val length: Float = 0.0f,
    @Json(name = "LocationX")
    val locationX: Float = 0.0f,
    @Json(name = "Visible")
    override val visible: Boolean = false,
    @Json(name = "ConnectedOEXIDs")
    val connectedOEXIDs: List<Int> = Collections.emptyList(),
    @Json(name = "StatusID")
    val statusID: String = "",
    @Json(name = "ID")
    val id: Int = 0,
    @Json(name = "Width")
    override val width: Float = 0.0f
) : GenericFloorObject()