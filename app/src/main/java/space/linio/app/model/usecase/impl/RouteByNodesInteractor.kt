package space.linio.app.model.usecase.impl

import io.reactivex.Single
import space.linio.app.model.repository.DataRepository
import space.linio.app.model.usecase.abs.RouteByNodesUseCase
import space.linio.app.presenter.ddo.RouteNodeData
import space.linio.app.util.NodeStubCreator
import javax.inject.Inject

class RouteByNodesInteractor
@Inject constructor(private val dataRepository: DataRepository) : RouteByNodesUseCase {
    override fun getRoute(fromNodeId: Int, toNodeId: Int): Single<List<RouteNodeData>> {
        return Single.just(
            listOf(
                NodeStubCreator.createNodeStub(), NodeStubCreator.createNodeStub(),
                NodeStubCreator.createNodeStub(), NodeStubCreator.createNodeStub(),
                NodeStubCreator.createNodeStub(), NodeStubCreator.createNodeStub()
            )
        )
    }
}