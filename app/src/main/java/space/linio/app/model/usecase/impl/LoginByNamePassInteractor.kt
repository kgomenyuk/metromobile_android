package space.linio.app.model.usecase.impl

import io.reactivex.Completable
import space.linio.app.di.UserAuthManager
import space.linio.app.model.repository.DataRepository
import space.linio.app.model.usecase.abs.LoginByNamePassUseCase

class LoginByNamePassInteractor(
    val repository: DataRepository,
    val userAuthManager: UserAuthManager
) : LoginByNamePassUseCase {
    override fun login(username: String, password: String): Completable {
        return repository
            .getToken(username, password)
            .doOnSuccess { token ->
                userAuthManager.login(username, token)
            }
            .toCompletable()
    }
}