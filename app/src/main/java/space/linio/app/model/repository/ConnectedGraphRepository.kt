package space.linio.app.model.repository

import space.linio.app.model.dto.data.graph.connected.Graph

/**
 * Created by tetawex on 02.03.2018.
 */
interface ConnectedGraphRepository {
    var graph: Graph
}