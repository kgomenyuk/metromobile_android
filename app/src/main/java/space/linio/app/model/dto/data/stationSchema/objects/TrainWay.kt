package space.linio.app.model.dto.data.stationschema.objects

import com.squareup.moshi.Json
import space.linio.app.model.dto.data.stationschema.RIType
import java.util.*

class TrainWay(
    @Json(name = "WidthAbs")
    val widthAbs: Int = 0,
    @Json(name = "ConnectedPlatformIDs")
    val connectedPlatformIDs: List<Int> = Collections.emptyList(),
    override val height: Float = 1f,
    @Json(name = "LengthAbs")
    val lengthAbs: Int = 0,
    @Json(name = "Visible")
    override val visible: Boolean = false,
    @Json(name = "StatusID")
    val statusID: String = "",
    @Json(name = "OrderNumber")
    override val orderNumber: Int = 0,
    @Json(name = "ID")
    val id: Int = 0,
    @Json(name = "RIType")
    override val rIType: RIType = RIType.TRAIN_WAY,
    @Json(name = "Code")
    override val code: String? = "",
    @Json(name = "Width")
    override val width: Float = 0f
) : GenericFloorObject()