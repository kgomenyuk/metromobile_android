package space.linio.app.model.api

/**
 * Created by tetawex on 13.04.2018.
 */
object PanoramaConfig {
    const val panoramaHeight = 768
    const val panoramaWidth = 4608

    const val photoHeight = 1280
    const val photoWidth = 768

    const val baseUrl = "https://linio.space/content/img_full/"

    const val imageTypePhoto = "ph_h"
    const val imageTypePanorama = "ph_pano"
}