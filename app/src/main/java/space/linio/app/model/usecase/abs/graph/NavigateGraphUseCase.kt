package space.linio.app.model.usecase.abs.graph

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import space.linio.app.model.dto.data.graph.connected.Node

/**
 * Created by Tetawex on 14.04.2018.
 */
interface NavigateGraphUseCase {

    val navigationUpdateEventEmitter: PublishSubject<NavigationUpdateEvent>

    fun getNodeById(id: Int): Single<Node>
    fun moveToNodeById(id: Int): Completable
    fun getCurrentNode(): Node
    fun goBack(): Completable
}

class NavigationUpdateEvent(val fromNode: Node, val toNode: Node)