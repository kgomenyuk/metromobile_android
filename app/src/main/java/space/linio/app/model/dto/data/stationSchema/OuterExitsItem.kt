package space.linio.app.model.dto.data.stationschema

import com.squareup.moshi.Json

data class OuterExitsItem(
    @Json(name = "GPSLatitude")
    val gPSLatitude: Double = 0.0,
    @Json(name = "NavStartPoints")
    val navStartPoints: List<NavStartPoint>,
    @Json(name = "Address")
    val address: String? = "",
    @Json(name = "NameEN")
    val nameEN: String? = "",
    @Json(name = "Visible")
    val visible: Boolean? = false,
    @Json(name = "StatusID")
    val statusID: String? = "",
    @Json(name = "ID")
    val id: Int? = 0,
    @Json(name = "RIType")
    val rIType: String? = "",
    @Json(name = "GPSLongitude")
    val gPSLongitude: Double? = 0.0,
    @Json(name = "Code")
    val code: String? = "",
    @Json(name = "Name")
    val name: String? = "",
    @Json(name = "AddressEN")
    val addressEN: String? = ""
)