package space.linio.app.model.api

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Tetawex on 29.10.2017.
 */

class ApiErrorInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        Log.d("errcode", response.code().toString())
        when {
            response.code() == 401 -> throw ApiException(ApiException.ErrorType.NOT_AUTHORISED)
            response.code() >= 500 -> throw ApiException(ApiException.ErrorType.SERVER)
            response.code() >= 400 -> throw ApiException(ApiException.ErrorType.CLIENT)
            else -> return response
        }

    }
}
