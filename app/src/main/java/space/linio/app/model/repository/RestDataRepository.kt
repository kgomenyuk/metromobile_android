package space.linio.app.model.repository

import io.reactivex.Single
import space.linio.app.model.api.ApiInterface
import space.linio.app.model.dto.data.stationschema.StationSchemaResponse
import space.linio.app.model.dto.request.LoginByNamePassRequest
import space.linio.app.model.dto.request.RegisterRequest
import space.linio.app.model.dto.response.RegisterResponse
import space.linio.app.presenter.ddo.RegisterCredentials

/**
 * Created by Tetawex on 31.10.2017.
 */

class RestDataRepository(private val apiInterface: ApiInterface) : DataRepository {
    override fun getCurrentStationSchema(): Single<StationSchemaResponse> {
        return apiInterface.getStationSchema(langId = "ru", stationId = 6212)
    }

    override fun registerUser(credentials: RegisterCredentials): Single<RegisterResponse> {
        return apiInterface.registerUser(
            RegisterRequest(
                login = credentials.login,
                password = credentials.password,
                username = credentials.username,
                email = credentials.email,
                roles = listOf("Chad Admin")
            )
        )
    }

    override fun getToken(username: String, password: String): Single<String> {
        return apiInterface
            .getToken(LoginByNamePassRequest(username, password))
            .map { response -> response.token }
    }
}
