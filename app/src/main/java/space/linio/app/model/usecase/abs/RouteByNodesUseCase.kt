package space.linio.app.model.usecase.abs

import io.reactivex.Single
import space.linio.app.presenter.ddo.RouteNodeData

/**
 * Created by Tetawex on 31.10.2017.
 */

interface RouteByNodesUseCase {
    fun getRoute(fromNodeId: Int, toNodeId: Int): Single<List<RouteNodeData>>
}