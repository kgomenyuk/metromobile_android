package space.linio.app.util

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader

object NULL_TO_EMPTY_STRING_ADAPTER {
    @FromJson
    fun fromJson(reader: JsonReader): String {
        if (reader.peek() != JsonReader.Token.NULL) {
            return reader.nextString()
        }
        reader.nextNull<Unit>()
        return ""
    }
}

object NULL_TO_ZERO_INT_ADAPTER {
    @FromJson
    fun fromJson(reader: JsonReader): Int {
        if (reader.peek() != JsonReader.Token.NULL) {
            return reader.nextInt()
        }
        reader.nextNull<Unit>()
        return 0
    }
}

object NULL_TO_ZERO_DOUBLE_ADAPTER {
    @FromJson
    fun fromJson(reader: JsonReader): Double {
        if (reader.peek() != JsonReader.Token.NULL) {
            return reader.nextDouble()
        }
        reader.nextNull<Unit>()
        return 0.0
    }
}

object NULL_TO_ZERO_FLOAT_ADAPTER {
    @FromJson
    fun fromJson(reader: JsonReader): Float {
        if (reader.peek() != JsonReader.Token.NULL) {
            return reader.nextDouble().toFloat()
        }
        reader.nextNull<Unit>()
        return 0f
    }
}

object NULL_TO_FALSE_BOOLEAN_ADAPTER {
    @FromJson
    fun fromJson(reader: JsonReader): Boolean {
        if (reader.peek() != JsonReader.Token.NULL) {
            return reader.nextBoolean()
        }
        reader.nextNull<Unit>()
        return false
    }
}