package space.linio.app.util

import org.joda.time.DateTime
import space.linio.app.presenter.ddo.RouteNodeData
import space.linio.app.presenter.ddo.RouteNodeType
import java.util.*

@Deprecated("to be removed when api comes online")
object NodeStubCreator {
    fun createNodeStub() = RouteNodeData(
        adjacentNodesList = Collections.emptyList<RouteNodeData>(),
        imageUrl = "",
        panoramaMarkList = Collections.emptyList(),
        estimatedArrivalTime = DateTime.now(),
        routeNodeType = RouteNodeType.TRAIN,
        description = "Зайдите в поезд до ст Пушкинская",
        nodeId = 1
    )
}