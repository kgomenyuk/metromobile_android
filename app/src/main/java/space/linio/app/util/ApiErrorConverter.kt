package space.linio.app.util

import android.content.Context
import retrofit2.HttpException
import space.linio.app.R
import java.io.IOException

/**
 * Created by Tetawex on 30.10.2017.
 */

object ApiErrorConverter {
    fun getStringMessage(context: Context, throwable: Throwable): String {
        if (throwable is HttpException)
            when (throwable.code()) {
                401 -> return context.getString(R.string.err_token_expired);
                500 -> return context.getString(R.string.err_server);
                400 -> return context.getString(R.string.err_client);
            }
        else if (throwable is IOException)
            return context.getString(R.string.err_no_internet)
        return context.getString(R.string.err_unknown)
    }
}
/*if (throwable instanceof ApiException) {
            ApiException.ErrorType errorType = ((ApiException) throwable).getErrorType();
            switch (errorType) {
                case SERVER:
                    return context.getString(R.string.err_server);
                case CLIENT:
                    return context.getString(R.string.err_client);
                case NOT_AUTHORISED:
                    return context.getString(R.string.err_token_expired);
            }*/
