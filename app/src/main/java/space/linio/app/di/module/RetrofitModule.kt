package space.linio.app.di.module

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import space.linio.app.BuildConfig
import space.linio.app.di.scope.PerApplication
import space.linio.app.model.dto.data.stationschema.objects.GenericFloorObject
import space.linio.app.model.dto.data.stationschema.objects.Platform
import space.linio.app.model.dto.data.stationschema.objects.TrainWay
import space.linio.app.model.dto.data.stationschema.objects.Wall
import space.linio.app.util.*

/**
 * Created by Tetawex on 29.10.2017.
 */

@Module(includes = [OkHttpModule::class])
class RetrofitModule {
    @Provides
    @PerApplication
    fun provideRetrofit(builder: Retrofit.Builder): Retrofit {
        val httpClient = OkHttpClient.Builder()
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(logging)
        return builder.baseUrl(BuildConfig.API_ENDPOINT).build()
    }

    @Provides
    @PerApplication
    fun provideRetrofitBuilder(converterFactory: Converter.Factory): Retrofit.Builder {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(converterFactory)
    }

    @Provides
    @PerApplication
    fun provideConverterFactory(moshi: Moshi): Converter.Factory {
        return MoshiConverterFactory.create(moshi).withNullSerialization()
    }

    @Provides
    @PerApplication
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(NULL_TO_EMPTY_STRING_ADAPTER)
            .add(NULL_TO_ZERO_INT_ADAPTER)
            .add(NULL_TO_ZERO_DOUBLE_ADAPTER)
            .add(NULL_TO_ZERO_FLOAT_ADAPTER)
            .add(NULL_TO_FALSE_BOOLEAN_ADAPTER)
            .add(
                RuntimeJsonAdapterFactory
                    .of(GenericFloorObject::class.java, "RIType")
                    .registerSubtype(TrainWay::class.java, "WAY")
                    .registerSubtype(Platform::class.java, "PLT")
                    .registerSubtype(Wall::class.java, "WAL")
            )
            .add(KotlinJsonAdapterFactory())
            .build()
    }
}