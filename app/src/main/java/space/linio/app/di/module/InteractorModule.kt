package space.linio.app.di.module

import dagger.Module
import dagger.Provides
import space.linio.app.di.UserAuthManager
import space.linio.app.model.repository.ConnectedGraphRepository
import space.linio.app.model.repository.DataRepository
import space.linio.app.model.repository.RawGraphRepository
import space.linio.app.model.usecase.abs.*
import space.linio.app.model.usecase.abs.graph.AppendGraphUseCase
import space.linio.app.model.usecase.abs.graph.ConnectGraphUseCase
import space.linio.app.model.usecase.abs.graph.NavigateGraphUseCase
import space.linio.app.model.usecase.impl.*
import space.linio.app.model.usecase.impl.graph.AppendGraphInteractor
import space.linio.app.model.usecase.impl.graph.ConnectGraphInteractor
import space.linio.app.model.usecase.impl.graph.NavigateGraphInteractor

@Module
class InteractorModule {

    @Provides
    fun getRawGraphByIdUseCase(rawGraphRepository: RawGraphRepository): RawGraphByIdUseCase {
        return RawGraphByIdInteractor(rawGraphRepository)
    }

    @Provides
    fun getAppendGraphUseCase(connectedGraphRepository: ConnectedGraphRepository): AppendGraphUseCase {
        return AppendGraphInteractor(connectedGraphRepository)
    }

    @Provides
    fun getGraphNavigationUseCase(connectedGraphRepository: ConnectedGraphRepository): NavigateGraphUseCase {
        return NavigateGraphInteractor(connectedGraphRepository)
    }

    @Provides
    fun getConnectGraphUseCase(): ConnectGraphUseCase {
        return ConnectGraphInteractor()
    }

    @Provides
    fun getRouteByNodesInteractor(dataRepository: DataRepository): RouteByNodesUseCase {
        return RouteByNodesInteractor(dataRepository)
    }

    @Provides
    fun getNodeByQueryInteractor(dataRepository: DataRepository): NodeByQueryUseCase {
        return NodeByQueryInteractor(dataRepository)
    }

    @Provides
    fun getStationDataByIdInteractor(dataRepository: DataRepository): StationDataByIdUseCase {
        return StationDataByIdInteractor(dataRepository)
    }

    @Provides
    fun getLoginByNamePassInteractor(
        dataRepository: DataRepository,
        userManager: UserAuthManager
    ): LoginByNamePassUseCase {
        return LoginByNamePassInteractor(dataRepository, userManager)
    }

    @Provides
    fun getRegisterInteractor(dataRepository: DataRepository): RegisterUseCase {
        return RegisterInteractor(dataRepository)
    }

    @Provides
    fun getCurrentStationSchemaInteractor(dataRepository: DataRepository): CurrentStationSchemaUseCase {
        return CurrentStationSchemaInteractor(dataRepository)
    }
}
