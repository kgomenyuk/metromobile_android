package space.linio.app.di.component.subcomponent

import dagger.Subcomponent
import space.linio.app.di.module.InteractorModule
import space.linio.app.di.scope.PerPresenter
import space.linio.app.presenter.TopDownPresenter

/**
 * Created by Tetawex on 29.10.2017.
 */
@Subcomponent(modules = [InteractorModule::class])
@PerPresenter
interface TopDownPresenterComponent {
    fun inject(presenter: TopDownPresenter)
}
