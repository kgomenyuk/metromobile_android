package space.linio.app.di

import android.preference.PreferenceManager
import space.linio.app.App
import space.linio.app.di.component.AppComponent
import space.linio.app.di.component.DaggerAppComponent
import space.linio.app.di.component.subcomponent.*
import space.linio.app.di.module.AppModule
import space.linio.app.di.module.RepositoryModule
import space.linio.app.di.module.UserModule
import space.linio.app.model.repository.PreferencesWrapper


/**
 * Created by Tetawex on 31.10.2017.
 */

class ComponentManager(app: App) {
    private val userAuthManager: UserAuthManager = UserAuthManager()
    private val appComponent: AppComponent = DaggerAppComponent.builder()
        .appModule(AppModule(app))
        .repositoryModule(RepositoryModule())
        .userModule(UserModule(userAuthManager))
        .build()

    val loginPresenterComponent: LoginPresenterComponent
    val registerPresenterComponent: RegisterPresenterComponent
    val navigationGraphComponent: NavigationGraphComponent

    var userComponent: UserComponent

    lateinit var navigatorPresenterComponent: NavigatorPresenterComponent
    lateinit var mapPresenterComponent: MapPresenterComponent
    lateinit var topDownPresenterComponent: TopDownPresenterComponent


    init {
        userComponent = appComponent.plusUserComponent()
        userAuthManager.userLoggedInListener = {
            userComponent = appComponent.plusUserComponent()
            initUserSubComponents()
        }
        userAuthManager.preferences =
            PreferencesWrapper(PreferenceManager.getDefaultSharedPreferences(app))
        loginPresenterComponent = appComponent.plusLoginPresenterComponent()
        registerPresenterComponent = appComponent.plusRegisterPresenterComponent()
        navigationGraphComponent = appComponent.plusNavigationGraphComponent()
    }

    private fun initUserSubComponents() {
        navigatorPresenterComponent = userComponent.plusNavigatorPresenterComponent()
        mapPresenterComponent = userComponent.plusMapPresenterComponent()
        topDownPresenterComponent = userComponent.plusTopDownPresenterComponent()
    }
}
