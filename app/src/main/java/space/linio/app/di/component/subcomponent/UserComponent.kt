package space.linio.app.di.component.subcomponent

import dagger.Subcomponent
import space.linio.app.di.module.SettingsModule
import space.linio.app.di.scope.PerUser

/**
 * Created by tetawex.
 */
@Subcomponent(modules = arrayOf(SettingsModule::class))
@PerUser
interface UserComponent {
    fun plusNavigatorPresenterComponent(): NavigatorPresenterComponent
    fun plusMapPresenterComponent(): MapPresenterComponent
    fun plusTopDownPresenterComponent(): TopDownPresenterComponent
}