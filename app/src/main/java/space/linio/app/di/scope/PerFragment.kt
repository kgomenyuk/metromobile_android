package space.linio.app.di.scope

import javax.inject.Scope

/**
 * Created by Tetawex on 29.10.2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerFragment
