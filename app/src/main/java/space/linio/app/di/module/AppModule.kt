package space.linio.app.di.module

import dagger.Module
import dagger.Provides
import space.linio.app.App
import space.linio.app.di.scope.PerApplication

/**
 * Created by Tetawex on 29.10.2017.
 */
@Module
class AppModule(
    @get:Provides
    @get:PerApplication
    val app: App
) {
}
