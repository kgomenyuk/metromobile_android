package space.linio.app.di.component.subcomponent

import dagger.Subcomponent
import space.linio.app.di.module.InteractorModule
import space.linio.app.di.scope.PerPresenter
import space.linio.app.model.usecase.abs.NodeByQueryUseCase
import space.linio.app.model.usecase.abs.RouteByNodesUseCase
import space.linio.app.presenter.RoutePresenter

/**
 * Created by Tetawex.
 */
@Subcomponent(modules = arrayOf(InteractorModule::class))
@PerPresenter
interface MapPresenterComponent {
    val nodeByQueryUseCase: NodeByQueryUseCase
    val routeByNodesUseCase: RouteByNodesUseCase
    fun inject(presenter: RoutePresenter)
}