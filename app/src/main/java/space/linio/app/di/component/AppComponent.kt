package space.linio.app.di.component

import dagger.Component
import space.linio.app.di.component.subcomponent.LoginPresenterComponent
import space.linio.app.di.component.subcomponent.NavigationGraphComponent
import space.linio.app.di.component.subcomponent.RegisterPresenterComponent
import space.linio.app.di.component.subcomponent.UserComponent
import space.linio.app.di.module.AppModule
import space.linio.app.di.module.RepositoryModule
import space.linio.app.di.module.UserModule
import space.linio.app.di.scope.PerApplication
import space.linio.app.model.repository.DataRepository

/**
 * Created by Tetawex on 29.10.2017.
 */
@PerApplication
@Component(modules = arrayOf(AppModule::class, RepositoryModule::class, UserModule::class))
interface AppComponent {
    var dataRepository: DataRepository

    fun plusUserComponent(): UserComponent

    fun plusLoginPresenterComponent(): LoginPresenterComponent
    fun plusRegisterPresenterComponent(): RegisterPresenterComponent
    fun plusNavigationGraphComponent(): NavigationGraphComponent
}
