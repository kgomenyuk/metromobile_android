package space.linio.app.di.module

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import space.linio.app.di.scope.PerApplication
import space.linio.app.model.api.ApiInterface

/**
 * Created by Tetawex on 29.10.2017.
 */
@Module(includes = arrayOf(RetrofitModule::class))
class ApiModule {
    @Provides
    @PerApplication
    fun createApiInterface(retrofit: Retrofit): ApiInterface {
        return retrofit.create(ApiInterface::class.java)
    }
}
