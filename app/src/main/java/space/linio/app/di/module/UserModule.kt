package space.linio.app.di.module

import dagger.Module
import dagger.Provides
import space.linio.app.di.UserAuthManager
import space.linio.app.di.scope.PerApplication

/**
 * Created by tetawex.
 */
@Module
class UserModule(private var userAuthManager: UserAuthManager) {
    @Provides
    @PerApplication
    internal fun getUserManager(): UserAuthManager {
        return userAuthManager
    }
}