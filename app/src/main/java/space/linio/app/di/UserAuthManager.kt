package space.linio.app.di

import space.linio.app.model.repository.PreferencesWrapper
import javax.inject.Inject

/**
 * Created by Tetawex on 09.01.18.
 */

class UserAuthManager() {
    @Inject
    lateinit var preferences: PreferencesWrapper

    var userLoggedInListener: () -> Unit = {}

    init {
    }

    fun logout() {
        preferences.logoutClear()
    }

    fun login(username: String, token: String) {
        preferences.login(username, token)
        userLoggedInListener.invoke()
    }
    //TODO: implement user switching logic and tie it to corresponding components and modules in component manager
}