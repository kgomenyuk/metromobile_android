package space.linio.app.di.component.subcomponent

import dagger.Subcomponent
import space.linio.app.di.module.InteractorModule
import space.linio.app.di.scope.PerService
import space.linio.app.service.NavigationGraphService

/**
 * Created by tetawex on 02.03.2018.
 */
@Subcomponent(modules = arrayOf(InteractorModule::class))
@PerService
interface NavigationGraphComponent {
    fun inject(target: NavigationGraphService)
}