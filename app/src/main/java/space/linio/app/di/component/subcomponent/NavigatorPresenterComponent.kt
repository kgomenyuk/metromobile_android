package space.linio.app.di.component.subcomponent

import dagger.Subcomponent
import space.linio.app.di.module.InteractorModule
import space.linio.app.di.scope.PerPresenter
import space.linio.app.model.usecase.abs.StationDataByIdUseCase
import space.linio.app.presenter.PanoramaPresenter

/**
 * Created by Tetawex on 29.10.2017.
 */
@Subcomponent(modules = arrayOf(InteractorModule::class))
@PerPresenter
interface NavigatorPresenterComponent {
    val stationByIdUseCase: StationDataByIdUseCase
    fun inject(presenter: PanoramaPresenter)
}
