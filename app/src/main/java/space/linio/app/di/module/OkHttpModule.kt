package space.linio.app.di.module

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import space.linio.app.di.scope.PerApplication
import space.linio.app.model.api.ApiErrorInterceptor

/**
 * Created by Tetawex on 29.10.2017.
 */

@Module
class OkHttpModule {
    @Provides
    @PerApplication
    fun provideHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        apiErrorInterceptor: ApiErrorInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(apiErrorInterceptor)
            .build()
    }

    @Provides
    @PerApplication
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return loggingInterceptor
    }

    @Provides
    @PerApplication
    fun provideApiErrorInterceptor(): ApiErrorInterceptor {
        return ApiErrorInterceptor()
    }
}