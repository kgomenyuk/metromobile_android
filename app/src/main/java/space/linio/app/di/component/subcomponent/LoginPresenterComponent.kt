package space.linio.app.di.component.subcomponent

import dagger.Subcomponent
import space.linio.app.di.module.InteractorModule
import space.linio.app.di.scope.PerPresenter
import space.linio.app.presenter.LoginPresenter

/**
 * Created by Tetawex.
 */
@Subcomponent(modules = arrayOf(InteractorModule::class))
@PerPresenter
interface LoginPresenterComponent {
    fun inject(presenter: LoginPresenter)
}