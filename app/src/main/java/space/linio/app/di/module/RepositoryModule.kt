package space.linio.app.di.module

import dagger.Module
import dagger.Provides
import space.linio.app.di.scope.PerApplication
import space.linio.app.model.api.ApiInterface
import space.linio.app.model.repository.*

/**
 * Created by Tetawex on 31.10.2017.
 */
@Module(includes = arrayOf(ApiModule::class, SettingsModule::class))
class RepositoryModule {
    @Provides
    @PerApplication
    internal fun rawDataRepository(apiInterface: ApiInterface): DataRepository {
        return RestDataRepository(apiInterface)
    }

    @Provides
    @PerApplication
    internal fun provideRawGraphRepository(
        apiInterface: ApiInterface,
        tokenProvider: TokenProvider
    ): RawGraphRepository {
        return RestCachingRawGraphRepository(apiInterface, tokenProvider)
    }

    @Provides
    @PerApplication
    internal fun connectedGraphRepository(): ConnectedGraphRepository {
        return RuntimeConnectedGraphRepository()
    }
}
