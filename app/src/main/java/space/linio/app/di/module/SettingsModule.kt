package space.linio.app.di.module

import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import space.linio.app.App
import space.linio.app.di.scope.PerApplication
import space.linio.app.model.repository.PreferencesWrapper
import space.linio.app.model.repository.TokenProvider

/**
 * Created by tetawex.
 */
@Module(includes = arrayOf(AppModule::class))
class SettingsModule {
    @Provides
    @PerApplication
    fun provideSharedPreferences(app: App): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }

    @Provides
    @PerApplication
    fun providePreferencesWrapper(preferences: SharedPreferences): PreferencesWrapper {
        return PreferencesWrapper(preferences)
    }

    @Provides
    @PerApplication
    fun provideTokenProvider(preferences: PreferencesWrapper): TokenProvider {
        return preferences
    }
}