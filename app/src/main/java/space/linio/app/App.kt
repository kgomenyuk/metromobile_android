package space.linio.app

import android.app.Application
import space.linio.app.di.ComponentManager

/**
 * Created by Tetawex on 29.10.2017.
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        componentManager = ComponentManager(this)

    }

    companion object {
        lateinit var componentManager: ComponentManager
    }
}
