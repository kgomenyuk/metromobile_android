package space.linio.app.view

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import space.linio.base.BaseView

/**
 * Created by tetawex.
 */
interface LoginView : BaseView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun navigateToMainScreen()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun navigateToRegisterScreen()
}