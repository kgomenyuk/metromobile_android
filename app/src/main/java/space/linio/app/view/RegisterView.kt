package space.linio.app.view

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import space.linio.base.BaseView

/**
 * Created by tetawex.
 */
interface RegisterView : BaseView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun navigateToLoginScreen()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessageUserAlreadyExists()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessageUserSuccessfullyRegistered()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessagePasswordDoesNotMatch()
}