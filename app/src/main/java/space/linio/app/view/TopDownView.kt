package space.linio.app.view

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import space.linio.app.model.dto.data.stationschema.Floor
import space.linio.base.BaseView

interface TopDownView : BaseView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setStationFloorSchema(floor: Floor)
}