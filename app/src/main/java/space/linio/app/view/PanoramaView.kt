package space.linio.app.view

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import space.linio.app.model.dto.data.graph.connected.Node
import space.linio.base.BaseView

/**
 * Created by Tetawex on 30.10.2017.
 */

interface PanoramaView : BaseView {
    //fun deviceRotationChanged(deviceRotation: Float)
    //fun setStationData(stationData: StationData)
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setDisplayNode(node: Node)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showCantGoBackHint()
}
