package space.linio.app.view

/**
 * Created by Tetawex on 29.10.2017.
 */

enum class ScreenTag {
    PANORAMA, TOP_DOWN, MAP, ROUTE, SETTINGS;


    companion object {

        fun toString(screenTag: ScreenTag): String {
            return when (screenTag) {
                MAP -> "map"
                ROUTE -> "route"
                SETTINGS -> "settings"
                PANORAMA -> "panorama"
                TOP_DOWN -> "top_down"
            }
        }
    }
}
