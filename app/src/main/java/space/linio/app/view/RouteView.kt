package space.linio.app.view

import space.linio.app.presenter.ddo.DisplayNodeData
import space.linio.base.BaseView


/**
 * Created by Tetawex on 30.10.2017.
 */

interface RouteView : BaseView {
    fun setNodeList(data: List<DisplayNodeData>)
    fun displayEmptyRouteHint()
    fun hideEmptyRouteHint()
}
