package space.linio.app.view

import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

import space.linio.base.BaseView

/**
 * Created by Tetawex on 29.10.2017.
 */

interface MainView : BaseView {
    @StateStrategyType(SingleStateStrategy::class)
    fun setScreen(screenTag: ScreenTag)
}
