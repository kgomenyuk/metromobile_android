package space.linio.base

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by tetawex on 11.07.17.
 */


interface BaseView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showProgressbar()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun hideProgressbar()

    @StateStrategyType(SkipStrategy::class)
    fun showError(throwable: Throwable)

}
