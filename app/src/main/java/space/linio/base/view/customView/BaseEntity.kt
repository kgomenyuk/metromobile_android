package space.linio.base.view.customView

abstract class BaseEntity(
    override val name: String = "",
    override var transform: Transform = Transform(),
    override var parent: Entity?,
    override var elevation: Float = 0f
) : Entity {
    override val pixelBounds
        get() = parent?.pixelBounds?.applyTransform(transform) ?: CanvasBounds.ZERO
}