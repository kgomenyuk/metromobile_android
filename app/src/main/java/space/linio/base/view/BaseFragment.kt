package space.linio.base.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import kotlinx.android.synthetic.main.view_progressbar.*
import space.linio.app.util.ApiErrorConverter
import space.linio.base.BaseView

abstract class BaseFragment : MvpAppCompatFragment(), BaseView {

    abstract val layoutId: Int

    open fun setupView(view: android.view.View): android.view.View {
        return view
    }

    abstract fun preInit()
    abstract fun postInit()

    override fun onCreate(savedInstanceState: Bundle?) {
        preInit()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): android.view.View? {
        val view = inflater.inflate(layoutId, container, false)
        return setupView(view)
    }

    override fun onStart() {
        postInit()
        super.onStart()
    }

    override fun showError(throwable: Throwable) {
        Toast.makeText(
            context,
            ApiErrorConverter.getStringMessage(context!!, throwable),
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun showProgressbar() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressbar() {
        progressbar.visibility = View.GONE
    }
}
