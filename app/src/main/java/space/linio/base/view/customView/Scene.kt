package space.linio.base.view.customView

import android.graphics.Canvas
import android.graphics.Rect
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.roundToInt

class Scene : Entity, CanvasDimensions {
    private val entities: MutableMap<String, Entity> = HashMap()
    private val drawableEntities: MutableList<DrawableEntity> = LinkedList()
    private val clickableEntities: MutableList<ClickableEntity> = LinkedList()

    override var contentOriginX = 0
        private set
    override var contentOriginY = 0
        private set

    override var contentWidth = 0
        private set
    override var contentHeight = 0
        private set

    override var contentCenterX = 0
        private set
    override var contentCenterY = 0
        private set

    override var elevation: Float
        get() = 0f
        set(_) = Unit

    private var _canvasBounds = CanvasBounds.ZERO
    override val pixelBounds: CanvasBounds
        get() = _canvasBounds

    override val name: String
        get() = "root"

    override var transform: Transform
        get() = Transform(Vector2(0f, 0f), Vector2(1f, 1f))
        set(_) = Unit

    override var parent: Entity?
        get() = null
        set(_) = Unit

    fun setContentSize(xOrigin: Int, yOrigin: Int, width: Int, height: Int) {
        contentWidth = width
        contentHeight = height
        contentOriginX = xOrigin
        contentOriginY = yOrigin
        resolveContentResize()
    }

    fun addEntity(entity: Entity) {
        entities[entity.name] = entity

        if (entity is ClickableEntity)
            clickableEntities.add(entity)
        if (entity is DrawableEntity) {
            drawableEntities.add(entity)
            drawableEntities.sortBy { it.elevation }
        }
    }

    fun removeEntity(name: String) {
        entities.remove(name)?.also {
            clickableEntities.remove(it)
            drawableEntities.remove(it)
        }

    }

    fun getEntity(name: String): Entity? {
        return entities[name]
    }

    fun draw(canvas: Canvas) {
        _canvasBounds = CanvasBounds(
            contentOriginX + (transform.position.x * contentWidth).roundToInt(),
            contentOriginY + (transform.position.y * contentHeight).roundToInt(),
            (transform.scale.x * contentWidth).roundToInt(),
            (transform.scale.y * contentHeight).roundToInt()
        )
        drawableEntities.forEach { it.draw(canvas) }
    }

    fun onClick(x: Int, y: Int) {
        clickableEntities.forEach { entity ->
            entity.run {
                val clickTarget = Rect(
                    (transform.position.x * contentWidth + contentOriginX).roundToInt(),
                    (transform.position.y * contentHeight + contentOriginY).roundToInt(),
                    (transform.position.x * contentWidth + contentOriginX +
                            transform.scale.x * contentWidth).roundToInt(),
                    (transform.position.y * contentHeight + contentOriginY +
                            transform.scale.y * contentHeight).roundToInt()
                )
                if (clickTarget.contains(x, y)) {
                    invokeOnClick(
                        ((x - contentOriginX) / contentWidth).toFloat(),
                        ((y - contentOriginY) / contentHeight).toFloat()
                    )
                }
            }
        }
    }

    fun clear() {
        entities.clear()
        clickableEntities.clear()
        drawableEntities.clear()
    }

    private fun resolveContentResize() {
        contentCenterX = contentWidth / 2 + contentOriginX
        contentCenterY = contentHeight / 2 + contentOriginY
    }
}