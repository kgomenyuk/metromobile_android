package space.linio.base.view.customView

data class Vector2(
    val x: Float = 0f,
    val y: Float = 0f
) {
    operator fun plus(vector2: Vector2) =
        Vector2(x + vector2.x, y + vector2.y)

    companion object {
        val ZERO = Vector2(0f, 0f)
    }
}