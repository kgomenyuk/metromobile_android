package space.linio.base.view.customView

data class Transform(
    var position: Vector2 = Vector2(),
    var scale: Vector2 = Vector2()
) {
    operator fun plus(transform: Transform) =
        Transform(position + transform.position, scale + transform.scale)

}