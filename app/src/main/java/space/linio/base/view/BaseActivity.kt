package space.linio.base.view

import android.os.Bundle
import android.view.View
import android.widget.Toast

import com.arellomobile.mvp.MvpAppCompatActivity
import kotlinx.android.synthetic.main.view_progressbar.*

import space.linio.app.util.ApiErrorConverter
import space.linio.base.BaseView


/**
 * Created by Tetawex on 30.10.2017.
 */

abstract class BaseActivity : MvpAppCompatActivity(), BaseView {

    abstract val layoutId: Int

    abstract fun setupView()

    abstract fun preInit()

    abstract fun postInit()

    public override fun onCreate(savedInstanceState: Bundle?) {
        preInit()
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        setupView()
        postInit()
    }

    public override fun onStart() {
        super.onStart()
        postInit()
    }

    override fun showProgressbar() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressbar() {
        progressbar.visibility = View.GONE
    }

    override fun showError(throwable: Throwable) {
        Toast.makeText(
            this,
            ApiErrorConverter.getStringMessage(this, throwable),
            Toast.LENGTH_SHORT
        ).show()
    }
}
