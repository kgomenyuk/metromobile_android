package space.linio.base.view.customView

/**
 * Created by tetawex on 08.08.2018.
 */
interface ClickableEntity : Entity {
    fun invokeOnClick(x: Float, y: Float)
    fun setOnClickListener(listener: (Float, Float) -> Unit)
}