package space.linio.base.view.customView

import android.graphics.Canvas

/**
 * Created by tetawex on 08.08.2018.
 */
interface DrawableEntity : Entity {
    fun draw(canvas: Canvas)
}