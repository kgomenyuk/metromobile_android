package space.linio.base.view

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*


abstract class BaseRecyclerAdapter<T, VH : androidx.recyclerview.widget.RecyclerView.ViewHolder>(private var context: Context) :
    androidx.recyclerview.widget.RecyclerView.Adapter<VH>() {
    private var inflater: LayoutInflater = LayoutInflater.from(context)
    private var data: MutableList<T> = ArrayList()

    protected abstract val layoutId: Int

    protected abstract fun bindSingleItem(viewHolder: VH, item: T)

    protected abstract fun createVH(view: View): VH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view = inflater.inflate(layoutId, parent, false)
        return createVH(view)
    }

    override fun onBindViewHolder(viewHolder: VH, position: Int) {
        bindSingleItem(viewHolder, data[position])
    }

    fun getData(): List<T> {
        return data
    }

    fun replaceData(data: List<T>) {
        this.data.clear()
        this.data.addAll(data)
    }

    fun appendData(data: List<T>) {
        this.data.addAll(data)
    }

    fun replaceDataWithNotify(data: List<T>) {
        replaceData(data)
        notifyDataSetChanged()
    }

    fun appendDataWithNotify(data: List<T>) {
        appendData(data)
        notifyDataSetChanged()
    }

    fun clear() {
        data.clear()
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return data.size
    }
}
