package space.linio.base.view.customView

interface CanvasDimensions {
    val contentWidth: Int
    val contentHeight: Int

    val contentOriginX: Int
    val contentOriginY: Int

    val contentCenterX: Int
    val contentCenterY: Int
}