package space.linio.base.view.customView

import kotlin.math.roundToInt

data class CanvasBounds(
    val x: Int = 0,
    val y: Int = 0,
    val width: Int = 0,
    val height: Int = 0
) {

    fun applyTransform(transform: Transform): CanvasBounds {
        return CanvasBounds(
            (x + width * transform.position.x).roundToInt(),
            (y + height * transform.position.y).roundToInt(),
            (width * transform.scale.x).roundToInt(),
            (height * transform.scale.y).roundToInt()
        )
    }

    companion object {
        val ZERO = CanvasBounds(0, 0, 0, 0)
    }
}