package space.linio.base.view.customView

import android.content.Context
import android.graphics.Canvas
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.AttributeSet
import android.view.View

open class SceneView(context: Context, attrs: AttributeSet?, defStyle: Int) :
    View(context, attrs, defStyle) {

    init {

    }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    protected var scene = Scene()

    private lateinit var sensorManager: SensorManager
    private lateinit var sensorEventListener: SensorEventListener
    private var deviceRotation: Float = 0f

    public override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        scene.setContentSize(
            width = width - paddingLeft - paddingRight,
            height = height - paddingTop - paddingBottom,
            xOrigin = paddingLeft,
            yOrigin = paddingTop
        )
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        scene.draw(canvas)
    }
}