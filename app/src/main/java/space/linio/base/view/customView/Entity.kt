package space.linio.base.view.customView

/**
 * Created by tetawex on 08.08.2018.
 */
interface Entity {
    val name: String
    var transform: Transform
    var parent: Entity?
    var elevation: Float
    val pixelBounds: CanvasBounds
}