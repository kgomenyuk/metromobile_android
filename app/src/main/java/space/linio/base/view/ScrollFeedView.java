package space.linio.base.view;

import java.util.List;

import space.linio.base.BaseView;

/**
 * Created by Tetawex on 29.07.2017.
 */

public interface ScrollFeedView<T> extends BaseView {
    void scrollToPosition(int position);

    void appendFeed(List<T> feed);

    void resetFeed();
}
