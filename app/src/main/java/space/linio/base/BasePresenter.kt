package space.linio.base

import com.arellomobile.mvp.MvpPresenter
import io.reactivex.Completable
import io.reactivex.CompletableTransformer
import io.reactivex.Single
import io.reactivex.SingleTransformer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

import space.linio.base.utils.RxUtils


/**
 * Created by Tetawex on 29.10.2017.
 */
abstract class BasePresenter<V : BaseView> : MvpPresenter<V>() {

    private val compositeDisposable = CompositeDisposable()

    protected fun addDisposable(disposable: Disposable): Disposable {
        compositeDisposable.add(disposable)
        return disposable
    }

    protected fun addDisposables(vararg disposables: Disposable) {
        compositeDisposable.addAll(*disposables)
    }

    protected fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    fun <T> composeSingleProgressbar(): SingleTransformer<T, T> {
        return SingleTransformer { tSingle ->
            tSingle.compose(
                RxUtils.composeSingleOpBeforeAndAfter<T>(
                    Runnable { viewState.showProgressbar() },
                    Runnable { viewState.hideProgressbar() })
            )
        }
    }

    fun composeCompletableProgressbar(): CompletableTransformer {
        return CompletableTransformer { tSingle ->
            tSingle.compose(
                RxUtils.composeCompletableOpBeforeAndAfter(
                    Runnable { viewState.showProgressbar() },
                    Runnable { viewState.hideProgressbar() })
            )
        }
    }

    //Kotlin extension functions
    fun <T> Single<T>.applyProgressbarBeforeAndAfter(): Single<T> {
        return this.compose(composeSingleProgressbar())
    }

    fun Completable.applyProgressbarBeforeAndAfter(): Completable {
        return this.compose(composeCompletableProgressbar())
    }
}
