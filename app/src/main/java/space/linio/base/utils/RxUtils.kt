package space.linio.base.utils

import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

object RxUtils {
    fun composeCompletableSchedulers(): CompletableTransformer {
        return CompletableTransformer { tObservable ->
            tObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }

    fun <T> composeSingleSchedulers(): SingleTransformer<T, T> {
        return SingleTransformer { tObservable ->
            tObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }

    fun <T> composeObservableSchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer { tObservable ->
            tObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }

    fun <T> composeSingleOpBeforeAndAfter(
        before: Runnable, after: Runnable
    ): SingleTransformer<T, T> {
        return SingleTransformer { tSingle ->
            tSingle
                .doOnSuccess { after.run() }
                .doOnError { after.run() }
                .doOnSubscribe { before.run() }
        }
    }

    fun composeCompletableOpBeforeAndAfter(
        before: Runnable, after: Runnable
    ): CompletableTransformer {
        return CompletableTransformer { tCompletable ->
            tCompletable
                .doOnComplete { after.run() }
                .doOnError { after.run() }
                .doOnSubscribe { before.run() }
        }
    }

    fun <T> composeObservableOpBeforeAndAfter(
        before: Runnable, after: Runnable
    ): ObservableTransformer<T, T> {
        return ObservableTransformer { tObservable ->
            tObservable
                .doOnComplete { after.run() }
                .doOnError { after.run() }
                .doOnSubscribe { before.run() }
        }
    }

    fun <T> composeObservableOpAfter(after: Runnable): ObservableTransformer<T, T> {
        return ObservableTransformer { tObservable -> tObservable.doOnComplete({ after.run() }) }
    }

    fun composeCompletableOpAfter(after: Runnable): CompletableTransformer {
        return CompletableTransformer { tObservable -> tObservable.doOnTerminate({ after.run() }) }
    }

    fun <T> composeDelayBefore(
        delay: Long = 200,
        timeUnit: TimeUnit = TimeUnit.MILLISECONDS
    ): SingleTransformer<T, T> {
        return SingleTransformer { tSingle ->
            tSingle.delaySubscription(delay, timeUnit, AndroidSchedulers.mainThread())
        }
    }
}