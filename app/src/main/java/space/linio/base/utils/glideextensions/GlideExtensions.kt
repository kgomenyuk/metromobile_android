package space.linio.base.utils.glideextensions

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide

/**
 * Created by tetawex on 25.07.2018.
 */

fun ImageView.loadImage(url: String, placeholderId: Int, context: Context) {
    Glide
        .with(context)
        .load(url)
        .asBitmap().centerCrop()
        .placeholder(placeholderId)
        .into(this)
}