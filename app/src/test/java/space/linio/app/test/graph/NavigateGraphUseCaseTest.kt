package space.linio.app.test.graph

import com.nhaarman.mockito_kotlin.whenever
import junit.framework.Assert.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import space.linio.app.model.dto.data.graph.connected.Edge
import space.linio.app.model.dto.data.graph.connected.Graph
import space.linio.app.model.dto.data.graph.connected.GraphBackStackIsEmptyException
import space.linio.app.model.dto.data.graph.connected.Node
import space.linio.app.model.repository.ConnectedGraphRepository
import space.linio.app.model.usecase.abs.graph.NavigateGraphUseCase
import space.linio.app.model.usecase.impl.graph.NavigateGraphInteractor
import space.linio.app.presenter.ddo.PanoramaDisplayData
import space.linio.app.test.rule.RuledTest
import space.linio.app.test.util.GraphHelpers
import java.util.*


class NavigateGraphUseCaseTest : RuledTest() {
    @Mock
    lateinit var graphRepository: ConnectedGraphRepository

    lateinit var useCase: NavigateGraphUseCase

    @Before
    fun init() {
        //Create the base graph
        val baseGraph = Graph().also {
            val node1 = Node(1, ArrayList<Edge>(), PanoramaDisplayData(""))
            val node2 = Node(2, ArrayList<Edge>(), PanoramaDisplayData(""))
            val node3 = Node(3, ArrayList<Edge>(), PanoramaDisplayData(""))

            GraphHelpers.connectNodes(node1, node2)
            GraphHelpers.connectNodes(node2, node1)
            GraphHelpers.connectNodes(node3, node1)

            it.nodes[1] = node1
            it.nodes[2] = node2
            it.nodes[3] = node3
        }
        //Set up the mock
        whenever(graphRepository.graph).thenReturn(baseGraph)

        useCase = NavigateGraphInteractor(repository = graphRepository)
        graphRepository.graph.currentNode = baseGraph.nodes[1]!!
    }

    @After
    fun dispose() {
    }

    @Test
    fun graph_initialized_currentNodeIdMatchesRequiredId() {
        assertEquals(1, useCase.getCurrentNode().id)
    }


    @Test
    fun graph_movedToNonexistentNode_currentNodeIsNotModified() {
        val node = useCase.getCurrentNode()
        useCase.moveToNodeById(-1000).subscribe()
        assertEquals(node, useCase.getCurrentNode())
    }

    @Test
    fun graph_movedToNode_currentNodeIdMatchesRequestedId() {
        useCase.moveToNodeById(2).subscribe()
        assertEquals(2, useCase.getCurrentNode().id)
    }

    @Test
    fun graph_subscribedToNavigationEventsAndMovedToAnotherNode_eventFired() {
        var fired = false
        useCase.navigationUpdateEventEmitter.subscribe { fired = true }
        useCase.moveToNodeById(2).subscribe()

        assertTrue(fired)
    }

    @Test
    fun graph_subscribedToNavigationEventsThenUnsubscribedAndMovedToAnotherNode_eventNotFired() {
        var fired = false
        val disposable = useCase.navigationUpdateEventEmitter.subscribe { fired = true }
        disposable.dispose()
        useCase.moveToNodeById(2).subscribe()

        assertFalse(fired)
    }

    @Test
    fun graph_movedForwardTwiceMovedBackTwice_returnedToInitialNode() {
        useCase.moveToNodeById(2).subscribe()
        useCase.moveToNodeById(3).subscribe()
        useCase.goBack().subscribe()
        useCase.goBack().subscribe()

        assertEquals(1, useCase.getCurrentNode().id)
    }

    @Test
    fun graph_movedWhenBackstackIsEmpty_stayedAtCurrentNodeAndThrownException() {
        val node = useCase.getCurrentNode()
        useCase.goBack().test().assertError(GraphBackStackIsEmptyException::class.java)
        assertEquals(node, useCase.getCurrentNode())
    }

    @Test
    fun graph_movedToNonexistentNodeWhenBackStackNotEmpty_backStackTopElementNotModified() {
        useCase.moveToNodeById(2).subscribe()
        val node = graphRepository.graph.nodeBackStack.peek()
        useCase.moveToNodeById(-1000).subscribe()
        assertEquals(node, graphRepository.graph.nodeBackStack.peek())
    }
}