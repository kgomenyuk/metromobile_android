package space.linio.app.test.graph

import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import space.linio.app.model.dto.data.graph.connected.Edge
import space.linio.app.model.dto.data.graph.connected.Graph
import space.linio.app.model.dto.data.graph.connected.Node
import space.linio.app.model.repository.ConnectedGraphRepository
import space.linio.app.model.usecase.abs.graph.AppendGraphUseCase
import space.linio.app.model.usecase.impl.graph.AppendGraphInteractor
import space.linio.app.presenter.ddo.PanoramaDisplayData
import space.linio.app.test.rule.RuledTest
import space.linio.app.test.util.GraphHelpers.connectNodes
import space.linio.app.test.util.GraphHelpers.nodeConnectedTo
import java.util.*

class AppendGraphUseCaseTest : RuledTest() {

    @Mock
    lateinit var graphRepository: ConnectedGraphRepository

    @Before
    fun init() {
        //Appending 1<-2->3 to 1->2 should produce 1<>2->3
        //Create a base graph
        val baseGraph = Graph().also {
            val node1 = Node(1, ArrayList<Edge>(), PanoramaDisplayData(""))
            val node2 = Node(2, ArrayList<Edge>(), PanoramaDisplayData(""))

            connectNodes(node1, node2)

            it.nodes[1] = node1
            it.nodes[2] = node2
        }
        //Create a new graph
        val newGraph = Graph().also {
            val node1 = Node(1, ArrayList<Edge>(), PanoramaDisplayData(""))
            val node2 = Node(2, ArrayList<Edge>(), PanoramaDisplayData(""))
            val node3 = Node(3, ArrayList<Edge>(), PanoramaDisplayData(""))

            connectNodes(node2, node3)
            connectNodes(node2, node1)

            it.nodes[1] = node1
            it.nodes[2] = node2
            it.nodes[3] = node3
        }

        //Set up the mock
        `when`(graphRepository.graph).thenReturn(baseGraph)

        val appendGraphUseCase: AppendGraphUseCase = AppendGraphInteractor(graphRepository)
        appendGraphUseCase.appendGraph(newGraph)
    }

    @After
    fun dispose() {
    }

    @Test
    fun graph_newGraphAppended_requiredConnectionsExist() {
        assertTrue(nodeConnectedTo(graphRepository.graph.nodes[1]!!, graphRepository.graph.nodes[2]!!) &&
                nodeConnectedTo(graphRepository.graph.nodes[2]!!, graphRepository.graph.nodes[1]!!) &&
                nodeConnectedTo(graphRepository.graph.nodes[2]!!, graphRepository.graph.nodes[3]!!
                ))
    }

    @Test
    fun graph_newGraphAppended_oddConnectionsAreAbsent() {
        assertFalse(nodeConnectedTo(graphRepository.graph.nodes[3]!!, graphRepository.graph.nodes[1]!!) &&
                nodeConnectedTo(graphRepository.graph.nodes[1]!!, graphRepository.graph.nodes[3]!!) &&
                nodeConnectedTo(graphRepository.graph.nodes[3]!!, graphRepository.graph.nodes[2]!!)
        )
    }
}