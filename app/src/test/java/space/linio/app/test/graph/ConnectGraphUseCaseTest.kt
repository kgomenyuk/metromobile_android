package space.linio.app.test.graph

import io.reactivex.Single
import junit.framework.Assert
import junit.framework.Assert.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import space.linio.app.model.dto.data.graph.Media
import space.linio.app.model.dto.data.graph.raw.RawEdge
import space.linio.app.model.dto.data.graph.raw.RawGraph
import space.linio.app.model.dto.data.graph.raw.RawNode
import space.linio.app.model.repository.RawGraphRepository
import space.linio.app.model.usecase.impl.graph.ConnectGraphInteractor
import space.linio.app.test.rule.RuledTest
import space.linio.app.test.util.GraphHelpers

class ConnectGraphUseCaseTest : RuledTest() {
    @Mock
    lateinit var rawGraphRepository: RawGraphRepository

    @Before
    fun init() {
        val nodes = mutableListOf(
                RawNode(nodeID = 1, media = listOf(Media(0))),
                RawNode(nodeID = 2, media = listOf(Media(0))),
                RawNode(nodeID = 3, media = listOf(Media(0))))

        val edges = mutableListOf(
                RawEdge(source = 1, target = 2),
                RawEdge(source = 2, target = 1),
                RawEdge(source = 2, target = 3))

        val rawGraph: RawGraph = RawGraph(nodes, edges)
        `when`(rawGraphRepository.getRawGraph(1)).thenReturn(Single.just(rawGraph))
    }

    @After
    fun dispose() {
    }

    @Test
    fun rawGraph_convertedToGraph_requiredConnectionsExist() {
        val connectGraphUseCase = ConnectGraphInteractor()
        rawGraphRepository.getRawGraph(1).subscribe { rawGraph ->
            val graph = connectGraphUseCase.connectGraph(rawGraph)
            assertTrue(GraphHelpers.nodeConnectedTo(graph.nodes[1]!!, graph.nodes[2]!!) &&
                    GraphHelpers.nodeConnectedTo(graph.nodes[2]!!, graph.nodes[1]!!) &&
                    GraphHelpers.nodeConnectedTo(graph.nodes[2]!!, graph.nodes[3]!!
                    ))
        }
    }

    @Test
    fun rawGraph_convertedToGraph_oddConnectionsAreAbsent() {
        val connectGraphUseCase = ConnectGraphInteractor()
        rawGraphRepository.getRawGraph(1).subscribe { rawGraph ->
            val graph = connectGraphUseCase.connectGraph(rawGraph)
            Assert.assertFalse(GraphHelpers.nodeConnectedTo(graph.nodes[3]!!, graph.nodes[1]!!) &&
                    GraphHelpers.nodeConnectedTo(graph.nodes[1]!!, graph.nodes[3]!!) &&
                    GraphHelpers.nodeConnectedTo(graph.nodes[3]!!, graph.nodes[2]!!)
            )
        }
    }
}