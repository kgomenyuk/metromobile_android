package space.linio.app.test.util

import space.linio.app.model.dto.data.graph.connected.Edge
import space.linio.app.model.dto.data.graph.connected.Node
import space.linio.app.model.dto.data.graph.connected.Vector2

/**
 * Created by Tetawex on 15.04.2018.
 */
object GraphHelpers {
    fun connectNodes(from: Node, to: Node) {
        val edge12 = Edge(
                fromNode = from,
                fromId = from.id,
                toNode = to,
                toId = to.id,
                displayPosition = Vector2(0f, 0f))
        from.edges.add(edge12)
        to.edges.add(edge12)
    }

    fun nodeConnectedTo(from: Node, to: Node): Boolean {
        from.edges.forEach {
            if (it.toId == to.id)
                return true
        }
        return false
    }
}